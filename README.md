The code for the MEMS multiplexer system can be found here:

https://bitbucket.org/expalpha/tpc_lasermux/

or on pi@aacpi

The executable is runMEMS, where the editable file is located in `test/runMEMS.cc`

To run, the executable requires 4 variables: the distance you want the laser to travel in x (`delta_x`), the distance you want the laser to travel in y (`delta_y`), the `stepsize`, and the time between stepsize (`timedelay`). The first two variables were necessary for running other procedures, and may be removed. The distance the laser travels is an integer value where 32 ~= 37.5 microns. The suggested stepsize should be kept smaller than 16, and the recommended time delay should be greater than 10 000 microseconds. An example of the command line input is:

     ./runMEMS -1024 2048 16 10000

`runMEMS` uses procedures written in `src/MEMS.cc`. These procedures are listed below. This code relies on `src/AD5666.cc`, `src/GPIOInterface.cc`, and `SpiInterface.cc`. The include files are labelled the same.

* `SetMidscale()` will place the laser at a central location (midscale = 0x3fff = 16 383)

* `GetV(double delta_x, double delta_y)` will ask the user to input new `delta_x` and `delta_y` values, then will ensure the values are within a safe range

* `CheckSafe(double delta_x, double delta_y)` will ensure the laser moves within a safe, pre-determined range. I have set this range to +/- 30 000 as that will mean the laser will not be directed past the ports.

* `SetVoltage(double delta_x, double delta_y)` will move the laser to the `delta_x` and `delta_y` positions specified earlier

* `Scan(double stepsize, double timedelay)` will move the laser over an area specified by the user and out a file. The user can use this txt file to see where the laser is at its highest intensity. The output file will have the name format: `ScanData_<stepize>_<timedelay>.txt`

* `ScanAndPlot(double stepsize, double timedelay)` only kind of works. It will run like `Scan`, but will output a plot using MatPlotLib. I found it much easier to run `Scan`, then plot the txt file using gnuplot. `ScanAndPlot` is currently commented from the code

* `Ramp(double delta_x, double delta_y, double stepsize, double timedelay)` is used to ramp the laser to the start positions in `SetMidscale`, `Scan`, and `ScanAndPlot`. It ensures the MEMS mirror is not jolted to reduce mechanical stress, and instead uses stepsize and timedelay to arrive at the start position.

* `FindPort()` will find the core of the fibre port that the user specifies, then go to that point. This part of the code needs some refining as the multiplexer system is finalized. An output file is also created using the name: `FindPort_<port number>.txt`

* `ShutDown()` will ramp the MEMS mirror back to midscale and remotely shut down the laser