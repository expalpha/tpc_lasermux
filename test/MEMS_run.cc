
/* To run:

   > ./runMEMS delta_x delta_y stepsize timedelay

   delta_x is the offset moved in the x-direction with respect to midscale (total movement is then midscale +/- delta_x)

   delta_y is the offset moved in the y-direction with respect to midscale (total movement is then midscale +/- delta_y)

   stepsize is the incremental step size to reach midscale+delta_x or midscale+delta_y

   timedelay is the time between step sizes (in microseconds). This should be greater than 10ms to account for the reverberation of the mirror */


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include "MEMS.hh"
#include "VoltageInput.hh"
#include <math.h>       /* sin */
#define PI 3.14159265

using std::cin;
using std::cerr;
using std::cout;
using std::endl;

MEMS mems;


int main(int argc, char **argv){
  int32_t x = 0, y = 0;
  bool demo = true;
  bool shutdown = false;
  if(argc > 2){
    x = atoi(argv[1]);
    y = atoi(argv[2]);
    demo = false;
    if(argc > 3)
      shutdown = (argv[3][0]=='q');
  }
  if(!mems.Init()){
    std::cerr << "Init error!" << std::endl;
    return 1;
  } else {
    cout << "Initialized!" << endl;
  }
  // mems.SwitchPD(true); //turns on laser

  mems.SwitchHV(true);
  sleep(1);
  mems.SetMidscale(); //sets initial starting position

  sleep(1);
  const uint16_t maxdiff = 100./200.*double(0xFFFF); // max. V difference is 150V
  cout << "maxdiff = " << std::dec <<  maxdiff << endl;
  // for(int i = 1; i < 16; i++){
  //     int32_t v = -maxdiff+2*i*maxdiff/16;
  //     cout << "v = " << std::dec << -maxdiff << " +2*" << i << " * " << maxdiff/16 << " = " << v << endl;
  //     mems.SetVoltage(v, v);
  //     sleep(5);
  // }

  if(demo){
    cout << "Ramping:" << endl;
    cout << "Corner 0:" <<  endl;
    mems.Ramp(-maxdiff, -maxdiff, 512);
    cout << "Corner 1:" <<  endl;
    mems.Ramp(maxdiff, -maxdiff, 512);
    cout << "Corner 2:" <<  endl;
    mems.Ramp(maxdiff, maxdiff, 512);
    cout << "Corner 3:" <<  endl;
    mems.Ramp(-maxdiff, maxdiff, 512);
    cout << "Corner 1:" <<  endl;
    mems.Ramp(maxdiff, -maxdiff, 512);
    cout << "Corner 0:" <<  endl;
    mems.Ramp(-maxdiff, -maxdiff, 512);
    cout << "Corner 2:" <<  endl;
    mems.Ramp(maxdiff, maxdiff, 512);

    cout << "Jumping:" << endl;
    cout << "Corner 0:" <<  endl;
    mems.SetCoordinates(-maxdiff, -maxdiff);
    sleep(1);
    cout << "Corner 1:" <<  endl;
    mems.SetCoordinates(maxdiff, -maxdiff);
    sleep(1);
    cout << "Corner 2:" <<  endl;
    mems.SetCoordinates(maxdiff, maxdiff);
    sleep(1);
    cout << "Corner 3:" <<  endl;
    mems.SetCoordinates(-maxdiff, maxdiff);
    sleep(1);
    cout << "Corner 1:" <<  endl;
    mems.SetCoordinates(maxdiff, -maxdiff);
    sleep(1);
    cout << "Corner 0:" <<  endl;
    mems.SetCoordinates(-maxdiff, -maxdiff);
    sleep(1);
    cout << "Corner 2:" <<  endl;
    mems.SetCoordinates(maxdiff, maxdiff);
    sleep(1);

    cout << "Lissajous demo" << endl;
    for(int i = 0; i < 5000; i++){
      double p = PI/100.*double(i);
      double x = maxdiff*cos(p);
      double y = maxdiff*sin(p);
      // cout << x << '\t' << y << endl;
      mems.SetCoordinates(x, y);
      usleep(20);
    }
    for(int i = 0; i < 5000; i++){
      double p = PI/100.*double(i);
      double x = maxdiff*cos(p);
      double y = maxdiff*sin(2*p);
      // cout << x << '\t' << y << endl;
      mems.SetCoordinates(x, y);
      usleep(20);
      // usleep(10000);
    }
    for(int i = 0; i < 5000; i++){
      double p = PI/100.*double(i);
      double x = maxdiff*cos(p);
      double y = maxdiff*sin(3*p);
      // cout << x << '\t' << y << endl;
      mems.SetCoordinates(x, y);
      usleep(20);
      // usleep(10000);
    }
    for(int i = 0; i < 5000; i++){
      double p = PI/100.*double(i);
      double x = maxdiff*cos((1.001)*p);
      double y = maxdiff*sin(3*p);
      // cout << x << '\t' << y << endl;
      mems.SetCoordinates(x, y);
      usleep(20);
      // usleep(10000);
      
    }
    mems.ShutDown(); //Ramps down to midscale, shuts off laser
  } else {
    mems.SetCoordinates(x,y);
  }
  // cout << "Enter something to shut down" <<  endl;
  // getchar();
  if(shutdown) mems.ShutDown(); //Ramps down to midscale, shuts off laser
  return(0);
}
