// spitest.cc
//
// Example program for bcm2835 library
// Shows how to interface with SPI to transfer a byte to and from an SPI device
//
// After installing bcm2835, you can build this
// with something like:
// gcc -o spi spi.c -l bcm2835
// sudo ./spi
//
// Or you can test it before installing with:
// gcc -o spi -I ../../src ../../src/bcm2835.c spi.c
// sudo ./spi
//
// Author: Mike McCauley
// Copyright (C) 2012 Mike McCauley
// $Id: RF22.h,v 1.21 2012/05/30 01:51:25 mikem Exp $

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include "SpiInterface.hh"

int main(int argc, char **argv)
{
    RaspberryPi::SpiInterface spi(true, 0, 2, 256);
    if(!spi.Init())
        {
            std::cerr << "Couldn't initialize." << std::endl;
            return 1;
        }

    uint32_t send_data = 0xbeef;
    uint32_t data_out = 12345;
    //uint32_t send_data = 0xffaa553;
    std::cout<<"send this"<<send_data<<std::endl;
    spi.Exchange(send_data,data_out);
    std::cout<<"return is"<<data_out<<std::endl;
    return 0;
}
