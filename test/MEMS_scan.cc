
/* To run:

 */

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <map>
#include <string>
#include <algorithm>    // std::sort
#include <chrono>
#include "MEMS.hh"
#include "PD.hh"

using std::cerr;
using std::cin;
using std::cout;
using std::endl;

MEMS mems;


std::pair<int16_t, int16_t> Scan(PD &pd, int32_t x0, int32_t y0, uint16_t width = 4000, uint16_t stepsize = 400, uint16_t timedelay = 50, std::string out_dir = "$HOME/tpc_lasermux/ScanData/")
{
  //Scans a predetermined area

  //Label the output file
  std::ostringstream oss;
  oss << out_dir <<"ScanData_x" << x0 << "_y" << y0 << "_" << stepsize << "_" << timedelay << ".txt"; //+ "_" +DateTime.Now.ToString("MM-dd-yyyy") +
  std::ofstream outputfile;
  outputfile.open(oss.str());
  // outputfile.open("ScanData_vertical_singlepass_left.txt");
  if (!outputfile)
    {
      std::cout << "Error opening file " << oss.str() << std::endl;
      return std::pair<int16_t, int16_t>(0, 0);
    }
  outputfile << "#x" << '\t' << "y" << '\t' << "V" << std::endl;

  //Set up initial values to detemine area of scan (max_x, max_y) and initial starting position

  int16_t x = x0 - width / 2;
  int16_t y = y0 - width / 2;
  if (x > mems.maxdiff)
    x = mems.maxdiff;
  else if (x < -mems.maxdiff)
    x = -mems.maxdiff;
  if (y > mems.maxdiff)
    y = mems.maxdiff;
  else if (y < -mems.maxdiff)
    y = -mems.maxdiff;
  mems.Ramp(x, y);

  int16_t xpeak = 0, ypeak = 0;
  double peakheight = 0;
  std::map<double,double> xval, yval;
  //Begin scan
  int nsteps = width / stepsize;
  using namespace std::chrono;
  steady_clock::time_point t0 = steady_clock::now();
  for (int j = 0; j <= nsteps; j++)
    {
      y = y0 - width / 2 + stepsize * (j + 1); //comment this for keeping y constant
      if (y > mems.maxdiff)
	break;
      else if (y < -mems.maxdiff)
	continue;

      for (int i = 0; i <= nsteps; i++)
        {
	  if (i > 0)
            {
	      if (j % 2)
		x -= stepsize;
	      else
		x += stepsize;
            }
	  if (x > mems.maxdiff)
	    break;
	  else if (x < -mems.maxdiff)
	    continue;
	  mems.SetCoordinates(x, y);
	  usleep(timedelay * 1000);
	  double voltage = 0.;
	  if(pd.acc == PD::scope_tek){
	    voltage = pd.GetV();
	  } else {
	    for(int k = 0; k < 3; k++){
	      voltage += pd.GetV();
	      usleep(timedelay * 1000);
	    }
	    voltage /= 3.;
	  }
	  outputfile << x << '\t' << y << '\t' << voltage << std::endl;
	  int totstep = j*nsteps+i;
	  steady_clock::time_point t = steady_clock::now();
	  duration<double> thetime = duration_cast<duration<double> >(t - t0);
	  double dt = thetime.count() / double(totstep) * double((nsteps+1)*nsteps);
	  std::cout << std::dec << j << '\t' << x << '\t' << y << '\t' << voltage << '\t' << std::setw(4) << totstep*100/((nsteps+1)*nsteps) << "%, finished in " << dt << " s" << std::endl;
	  xval[x] += voltage;
	  yval[y] += voltage;
	  //std::cout<<"voltage is: " <<voltage<<std::endl;
	  if (voltage > peakheight)
            {
	      peakheight = voltage;
	      xpeak = x;
	      ypeak = y;
            }
        }
    }

  auto xmax = *std::max_element(xval.begin(), xval.end(),
				[](const std::pair<double, double>& p1, const std::pair<double, double>& p2) {
				  return p1.second < p2.second; });
  auto xmin = *std::min_element(xval.begin(), xval.end(),
				[](const std::pair<double, double>& p1, const std::pair<double, double>& p2) {
				  return p1.second < p2.second; });

  double thres = xmin.second + 0.2*(xmax.second-xmin.second);

  double valsum = 0;
  double xmean = 0;
  for(auto v: xval){
    if(v.second > thres){
      valsum += v.second;
      xmean += v.second*v.first;
    }
  }
  xmean /= valsum;

  auto ymax = *std::max_element(yval.begin(), yval.end(),
				[](const std::pair<double, double>& p1, const std::pair<double, double>& p2) {
				  return p1.second < p2.second; });
  auto ymin = *std::min_element(yval.begin(), yval.end(),
				[](const std::pair<double, double>& p1, const std::pair<double, double>& p2) {
				  return p1.second < p2.second; });

  thres = ymin.second + 0.2*(ymax.second-ymin.second);

  valsum = 0;
  double ymean = 0;
  for(auto v: yval){
    if(v.second > thres){
      valsum += v.second;
      ymean += v.second*v.first;
    }
  }
  ymean /= valsum;

  outputfile.close();
  std::cout << "Done scanning, output saved to " << oss.str() << std::endl;
  std::cout << "Maximum at:       " << xpeak << ", " << ypeak << endl;
  std::cout << "Weighted mean at: " << xmean << ", " << ymean << endl;
  return std::pair<int16_t, int16_t>(xmean, ymean);
}

int main(int argc, char **argv)
{
  if(argc < 4){
    cerr << "Usage: MEMS_scan <pdtype> <x> <y> [output_dir] [width] [stepsize] [delay]" << endl;
    return 1;
  }

  PD::accesstype pdtype = (argv[1][0]=='p')?PD::phidget:PD::scope_tek;
  int32_t x0 = atoi(argv[2]);
  int32_t y0 = atoi(argv[3]);

  uint16_t width = 10000; 
  uint16_t stepsize = 100;
  uint16_t timedelay = 50;
  std::string out_dir = "ScanData";

  if(argc > 4)
    out_dir = argv[4];
  if(argc > 5)
    width = atoi(argv[5]);
  if(argc > 6)
    stepsize = atoi(argv[6]);
  if(argc > 7)
    timedelay = atoi(argv[7]);

  if (!mems.Init())
    {
      std::cerr << "Init error!" << std::endl;
      return 1;
    }
  else
    {
      cout << "Initialized!" << endl;
    }
  mems.SwitchPD(true); //turns on PD power
  PD pd(pdtype);
  pd.SetVerbose(0);
  if(pd.Init(16)){
    std::cout << "PD initialized" << endl;
  } else {
    cout << "Couldn't initialize PD" << endl;
    return 2;
  }

  mems.SwitchHV(true);
  sleep(1);
  mems.SetMidscale(); //sets initial starting position

  sleep(1);

  auto peakpos = Scan(pd, x0, y0, width, stepsize, timedelay, out_dir);
  cout << "Peak found at " << peakpos.first << ", " << peakpos.second << endl;
  mems.ShutDown(); //Ramps down to midscale, shuts off laser
  return (0);
}
