
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <phidget22.h>
#include "VoltageInput.hh"
#include <cassert>
#include <fstream>
#include <vector>
#include <ctime>
#include <chrono>
#include <sys/time.h>
//#include "GPIOInterface.hh"
//#include "SpiInterface.hh"
#include "AD5666.hh"

#define DAC_ALL 15

int main(int argc, char **argv)
{
  AD5666 dac(0); 
  if(!dac.Init())
    {
      std::cerr << "Couldn't initialize." << std::endl;
      return 0;
    }
  RaspberryPi::GPIOInterface gpio;
  RaspberryPi::SpiInterface spi(true, 0, 2, 256);

  /////////////////////////////////////////////////////////
  dac.gpio.SetPin(8, true);
  //suseconds_t time = 0;
  
  dac.SetIntRefDaisy(true, true);
  //std::cout<<"Running Time Plot Test"<<std::endl;
  //uint32_t send_data = 9 << 24; //00000000
  //uint32_t data_out = 12345;
  uint16_t midscale = 0x3fff;
  //double yvoltage = 1280;
  //double xvoltage = -128;
  //double xpos = midscale + xvoltage; 
  //double ypos = midscale + yvoltage;
  double timedelay = 10000; //10000
  //dac.SetDACReg(DAC_ALL,0);
  //dac.Apply();
  //spi.Exchange(send_data,data_out);
  //std::cout<<"All voltages set to: "<<data_out<<std::endl;


  //////////////////////////////////////////////////////
  /////////////   Time Plot Test   /////////////////////
  //////////////////////////////////////////////////////
  //std::chrono::seconds sec(1);
  //std::ofstream outputfile;
  //outputfile.open("TimeTest_x-128.txt");
   //if (!outputfile)
       //{
       //std::cout << "Error opening file" << std::endl;
       //return -1;
       //}
     //outputfile << "time" << '\t' << "#x position" << '\t' <<  "y position" << '\t' << " Voltage " << std::endl;
     //VoltageInput vi;
   //vi.SetDataInterval(vi.GetMinDataInterval());
  
   //dac.SetDACReg(0, midscale);
   //dac.SetDACReg(3, midscale);
  //dac.SetDACReg(DAC_ALL, midscale);
  //dac.Apply();
  //spi.Exchange(send_data,data_out);
  //std::cout<<"All voltages set to: "<<data_out<<std::endl;
  //dac.SetDACReg(2, midscale + yvoltage);
  //dac.SetDACReg(1, midscale + xvoltage);
  //dac.Apply();
   
  //for (int i = 0; i < 100; i++){
     //double voltage = vi.GetVoltage();
     //time += std::chrono::microseconds(sec).count();
     //high_resolution_clock::time_point t1 = high_resolution_clock::now();
     //outputfile << time  << '\t' << xpos << '\t' << ypos << '\t' << voltage << std::endl;
     //usleep(timedelay);
     //}
   //outputfile.close();
   // dac.gpio.SetPin(8, false);
  //spi.Exchange(send_data,data_out);
  //std::cout<<"At y_drive, the voltage is: "<<data_out<<std::endl;
  //dac.SetDACReg(1, midscale + dvoltage);
  //dac.Apply();
  //spi.Exchange(send_data,data_out);
  //std::cout<<"At x_drive, the voltage is: "<<data_out<<std::endl;


  
  //uint8_t addr = 0;
  //dac.SetDACReg(addr,0x2c00);
  //dac.Apply();
  //spi.Exchange(send_data,data_out);
  //std::cout<<"At addr "<<addr<<" the voltage is: "<<data_out<<std::endl;
  // for(int j=0; j<4; j++){
  //dac.SetDACReg(j, voltage);
  //dac.Apply();
  //spi.Exchange(send_data,data_out);
  //std::cout<<"At addr "<<j<<" the voltage is: "<<data_out<<std::endl;
  //}
  
  //uint32_t send_data = 0x90; //10010000
  //uint32_t data_out = 12345;
  //spi.Exchange(send_data, data_out);
  //std::cout<<"return value is :"<<data_out<<std::endl;
  /////// Lars:
  // for(int i = 0; i < 10; i++){
  // // dac.SetDACReg(15,0); //Set all DAC to zero
  //   for(int j = 0; j < 4; j++){
  //     dac.SetDACReg(j, 0);
  //     usleep(100000);
  //   }
  // dac.Apply();
  // sleep(1);
  // uint16_t midscale = 0x3fff;
  // // dac.SetDACReg(DAC_ALL, midscale); //Set all DAC channels to midscale
  // for(int j = 0; j < 4; j++){
  //     dac.SetDACReg(j, midscale);
  //     usleep(100000);
  //   }
  // dac.Apply();
  // sleep(1);
  // usleep(100000);
  // }
  //////////////////////////////////////////

  //////// Pooja:
  //std::cout<<"Testing"<<std::endl;
  //dac.SetDACReg(DAC_ALL, 0);
  //dac.Apply();
  // sleep(5);
  //spi.Exchange(send_data, data_out);
  //std::cout<<"All voltages should be midscale: "<<data_out<<" but are they? Note that midscale is "<<midscale<<std::endl;
  // double setvoltage1 = 0;//x drive
  // double setvoltage2 = 0; //y drive
  // double setvoltage0 = midscale;
  // double setvoltage3 = midscale;
  // std::cout << "Setting Y" << std::endl;
  // dac.SetDACReg(2,setvoltage2);
  // dac.Apply();
  //sleep(5);
  // //spi.Exchange(send_data,data_out);
  // //std::cout<<"y drive is set to "<<setvoltage2<<" and it's reading "<<data_out<<std::endl;
  // std::cout << "Setting X" << std::endl;
  // dac.SetDACReg(1,setvoltage1);
  // dac.Apply();
  // sleep(5);

  //////////////////////////////////////////////
  ///////// Test range by making a square //////
  /////////////////////////////////////////////.
  // std::cout << "Setting Xmin" << std::endl;
  // dac.SetDACReg(1,100);
  // dac.SetDACReg(2,1000);
  // dac.Apply();
  // sleep(5);
  // dac.SetDACReg(2,2*midscale-1000);
  // dac.Apply();
  // sleep(5);
  // std::cout << "Setting Xmax" << std::endl;
  // dac.SetDACReg(1,2*midscale-100);
  // dac.Apply();
  // sleep(5);
  // dac.SetDACReg(2,1000);
  // dac.Apply();

  ///////////////////////////////////////////
  
  //spi.Exchange(send_data, data_out);
  //std::cout<<"x drive is set to "<<setvoltage1<<" and it's reading "<<data_out<<std::endl;
  //dac.SetDACReg(0, setvoltage0);
  //dac.Apply();
  //spi.Exchange(send_data, data_out);
  //std::cout<<"y offset is set to "<<setvoltage0<<" and it's reading "<<data_out<<std::endl;
  //dac.SetDACReg(3, setvoltage3);
  //dac.Apply();
  //spi.Exchange(send_data, data_out);
  //std::cout<<"x offset is set to "<<setvoltage3<<" and it's reading "<<data_out<<std::endl;
  //std::cout<<"Done testing"<<std::endl;
  

  
  // uint16_t setvoltage = midscale;
  // dac.SetDACReg(DAC_ALL, setvoltage); //Set Y Drive
  // dac.SetDACReg(1, setvoltage); //Set X Drive

  // ///////////////////////////////////////////////
  // dac.SetDACReg(0, midscale); //Set Y offset 
  // dac.SetDACReg(3, midscale); //Set X offset

  //dac.Apply();
  std::cout<<"send_data command"<<std::endl;
  uint32_t send_data = 0xbeef;//0xffaa5533;
  spi.Send(send_data);
  gpio.SetPin(15,true);

 
  return 0;
}




// Input.......Addr
// Y_o.........0
// X_D.........1
// Y_D.........2
// X_o.........3
