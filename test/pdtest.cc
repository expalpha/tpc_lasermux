#include <iostream>

#include "PD.hh"

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv){
  if(argc < 2){
    cerr << "Usage: " << argv[0] << " <s/p>" << endl;
    cerr << "       " << "s/p selects scope or phidget" << endl;
    return 1;
  }
  PD::accesstype pdtype = (argv[1][0]=='p')?PD::phidget:PD::scope_tek;
  PD pd(pdtype);
  if(pd.Init(16)){
    std::cout << "PD initialized" << endl;
  } else {
    cout << "Couldn't initialize PD" << endl;
    return 2;
  }
   
  double v = 0.;
  for(int i = 0; i < 10; i++){
    v = pd.GetV();
    cout << i << '\t' << v << endl;
  }
  return 0;
}
