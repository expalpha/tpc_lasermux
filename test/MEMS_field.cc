
/* To run:

 */

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <map>
#include <string>
#include <algorithm>    // std::sort
#include "MEMS.hh"
#include "PD.hh"

using std::cerr;
using std::cin;
using std::cout;
using std::endl;

MEMS mems;


void UVOutput(PD &pd)
{
  mems.verbose = 2;
  //Scans a predetermined area
  int16_t x = 26800;//19500 T03;
  int16_t y = -14100;//17300 T03;
  uint64_t timedelay = 5*60; //5 min or 300s
  uint64_t Bramptime = 75*60; //75 min or 300s
  //Label the output file
  
  std::string out_dir = "../../ScanData/";
  std::ostringstream oss;
  oss << out_dir <<"UVOutput_x" << x << "_y" << y << "_14.10.2022.txt";
  std::ofstream outputfile;
  outputfile.open(oss.str());
  if (!outputfile)
    {
      std::cout << "Error opening file " << oss.str() << std::endl;
    }
  outputfile << "#time" << '\t' << "V" << std::endl;

  mems.Ramp(x, y);
  for (uint64_t i = 0; i <= Bramptime; i += timedelay){
    double voltage = 0.;
    if(pd.acc == PD::scope_tek){
      voltage = pd.GetV();
    } else {
      for(int k = 0; k < 3; k++){
	voltage += pd.GetV();
	usleep(timedelay * 1000);
      }
      voltage /= 3.;
    }
    usleep(timedelay*1000000);//wait for 5min (in us)
    outputfile << i << '\t' << voltage << std::endl;
  }

  outputfile.close();
  std::cout << "Done scanning, output saved to " << oss.str() << std::endl;

}

int main(){
  PD::accesstype pdtype = PD::scope_tek;
  //Initialize MEMS and turn on PD
  if (!mems.Init())
    {
      std::cerr << "Init error!" << std::endl;
    }
  else
    {
      cout << "Initialized!" << endl;
    }
  mems.SwitchPD(true);

  PD pd(pdtype);
  pd.SetVerbose(1);
  if(pd.Init(16)){
    std::cout << "PD initialized" << endl;
  } else {
    cout << "Couldn't initialize PD" << endl;
  }
  mems.SwitchHV(true);
  //ramp to port

  UVOutput(pd);
  mems.ShutDown();
}
