#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "GPIOInterface.hh"

int main(int argc, char **argv)
{
  RaspberryPi::GPIOInterface gpio;
  if(!gpio.Init())
    {
      std::cerr << "Couldn't initialize." << std::endl;
      return 1;
    }

  gpio.GpClockOn();
  for(int i = 0; i < 10; i++){
      gpio.SetPin(15, true);
      std::cout << (int)gpio.ReadPin(13) << std::endl;
      usleep(1);
      gpio.SetPin(15, false);
      std::cout << (int)gpio.ReadPin(13) << std::endl;
  }
  return 0;
}
