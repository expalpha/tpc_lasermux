#include <iostream>

#include "TektronixScope.hh"

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv){
   TektronixScope scope;
   if(!scope.Connect()){
      cerr << "Couldn't connect to scope" << endl;
      return 1;      
   }
   scope.SetSettings(1, 100.e-9);
   
   double v = scope.GetV();
   cout << "Pulse height: " << v << endl;
   return 0;
}
