#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "GPIOInterface.hh"
#include "SpiInterface.hh"

int main(int argc, char **argv)
{
  RaspberryPi::GPIOInterface gpio;
  if(!gpio.Init())
    {
      std::cerr << "Couldn't initialize GPIO." << std::endl;
      return 1;
      std::cout<< "gpio didn't initialize" <<std::endl;
    }
  RaspberryPi::SpiInterface spi(true, 0, 2, 256);
  if(!spi.Init())
    {
      std::cerr << "Couldn't initialize SPI." << std::endl;
      return 2;
      std::cout<< "spi didn't initialize" <<std::endl;
    }

  gpio.SetPin(15, true);
  std::cout<< "set pin 15 for some reason" <<std::endl;

  uint32_t send_data = 0xffaa5533;
  spi.Send(send_data);

  gpio.SetPin(15, false);
  gpio.SetPin(15, true);
  return 0;
  std::cout<< "end of full test" <<std::endl;
}
