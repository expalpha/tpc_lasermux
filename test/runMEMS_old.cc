
/* To run:

> ./runMEMS_old delta_x delta_y stepsize timedelay

delta_x is the offset moved in the x-direction with respect to midscale (total movement is then midscale +/- delta_x)

delta_y is the offset moved in the y-direction with respect to midscale (total movement is then midscale +/- delta_y)

stepsize is the incremental step size to reach midscale+delta_x or midscale+delta_y

timedelay is the time between step sizes (in microseconds). This should be greater than 10ms to account for the reverberation of the mirror */


#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "MEMS_old.hh"

#define DAC_ALL 15
#define midscale 0x3fff

//need a way to display correct arguments if user inputs the incorrect arguments

int main(int argc, char **argv){

  MEMS_old mems;
  if(!mems.Init()){
    std::cerr << "Init error!" << std::endl;
    return 1;
  }
  mems.SwitchPD(true); //turns on laser

  mems.SetMidscale(); //sets initial starting position
  usleep(50000); //allows MEMS_old mirror to settle

  if(argc == 1){
    // while(true)
    //  mems.SetCoordinates(); //only doing this for testing. Use FindPort instead
     mems.FindPort(); //Finds a port and goes to it
  } else if(argc == 2) {
    mems.FindPort(atoi(argv[1]));
  } else if(argc == 3) {
    double delta_x = atoi(argv[1]);
    double delta_y = atoi(argv[2]);
    // double stepsize = atoi(argv[3]);
    // double timedelay = atoi(argv[4]);   
    double stepsize = 64;
    double timedelay = 1000;
    //mems.CheckSafe(delta_x,delta_y); //determines whether delta_x and delta_y are within a safe usage range
    mems.Ramp(delta_x+4*stepsize,delta_y,stepsize, timedelay);
    mems.Ramp(delta_x,delta_y,stepsize, timedelay);
    //mems.Scan(stepsize, timedelay); //Scans an area for highest intensity
  }
  //mems.ShutDown(); //Ramps down to midscale, shuts off laser
  return(0);
}
