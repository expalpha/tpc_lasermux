#include <stdio.h>
#include <iostream>
#include <iomanip>
#include "AD5666.hh"

int main(int argc, char **argv)
{
  AD5666 dac(0);
  if(!dac.Init())
    {
      std::cerr << "Couldn't initialize." << std::endl;
      return 1;
    }
  std::cout << "Initialized." << std::endl;
  dac.SetIntRefDaisy(true, true);
  // dac.SendCommand(10, AD_ALL_DACS, 0x55);
  //  dac.PulseLDAC();
  //dac.SetIntRefDaisy(true, false);
  dac.SetDACReg(2, 0xefff);
  dac.Apply();;
  //dac.SendCommand(0001,1111);
  return 0;
}
