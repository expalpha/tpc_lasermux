/*
  ALPHA › ICE450 laser controller frontend
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <dirent.h>

#include <chrono>

#include <cassert>

#include "tmfe_rev0.h"
#include "midas.h"

#include "MEMS.hh"

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;

/*
  static void WVD(TMFE* mfe, TMFeEquipment* eq, const char* name, int num, const double v[])
  {
  if (mfe->fShutdown)
  return;

  std::string path;
  path += "/Equipment/";
  path += eq->fName;
  path += "/Variables/";
  path += name;
  //printf("Write ODB %s Readback %s: %s\n", C(path), name, v);
  int status = db_set_value(mfe->fDB, 0, path.c_str(), &v[0], sizeof(double)*num, num, TID_DOUBLE);
  if (status != DB_SUCCESS) {
  printf("WVD: db_set_value status %d\n", status);
  }
  }
*/

static vector<string> FindTtyUSB(){
   DIR *dp;
   struct dirent *ep;
   vector<string> hits;
   dp = opendir ("/dev/");
   if (dp != NULL)
      {
         while ((ep = readdir (dp))){
            string f(ep->d_name);
            if(f.substr(0,6).compare("ttyUSB") == 0){
               f.insert(0,"/dev/");
               hits.push_back(f);
            }
         }
         (void) closedir (dp);
      }
   else
      perror ("Couldn't open the directory");
   return hits;
}

class QLaser: public TMFeRpcHandlerInterface
{
public:
   TMFE* mfe = NULL;
   TMFeEquipment* eq = NULL;
   //MVOdb* fOdb = NULL;
   MVOdb* fS = NULL; // Settings
   MVOdb* fV = NULL; // Variables

   int flash, qs, atten, freq, qsdelay, port;
   int standby = 30;
   bool pumpstate = true;
   // std::fstream sp;
   struct termios tio_ice, tio_mvat;
   int tty_ice, tty_mvat;
   std::chrono::time_point<std::chrono::system_clock> last_on;
   QLaser();
   ~QLaser(){
      if(!Stop()){
         mfe->Msg(MERROR, "~QLaser", "Laser shutdown command failed! Laser in unknown state, may be running!");
      }
      if(have_mvat){
         if(SetAtt(0)) mfe->Msg(MINFO, "Stop", "Attenuator fully closed.");
         else mfe->Msg(MERROR, "Stop", "Attenuator doesn't want to be reset.");
         close(tty_mvat);
      }
      if(mems){
         mems->ShutDown();
         delete mems;
      }
      eq->SetStatus("Frontend stopped", "#FF0000");
      close(tty_ice);
      mfe->Disconnect();
   };

   bool Init();
   bool InitMEMS();
   void ReleaseMEMS();
   bool CheckConnection();
   bool CheckConnectionIce();
   bool CheckConnectionMvat();
   string CheckInterlock();
   bool Connect();
   bool Stop();
   bool SetAtt(int att);
   string Status();

   bool Simmer();
   bool StartFlash();
   bool StartQS();
   bool StopQS();
   bool SetFreq(int freq);
   int GetFreq();
   bool SetDelay(int delay);
   int GetDelay();
   int GetFLCount();
   vector<double> GetTemps();
   int CoolantLvlOK();
   double GetFlow();
   void GetVars();
   int GetPumpState();
   int SetPumpState(bool state);

   bool SetPort(short i);

   bool ResetODB(bool att = false); // att = true also sets attenuator to zero

   std::string HandleRpc(const char* cmd, const char* args);

   vector<string> ExchangeIce(string cmd);
   string ExchangeMvat(string cmd);

private:
   bool have_mvat = false;
   MEMS *mems = nullptr;
   vector<int> portX, portY;
};


QLaser::QLaser(){
   mfe = TMFE::Instance();
   memset(&tio_ice,0,sizeof(tio_ice));
   tio_ice.c_iflag=0;
   tio_ice.c_oflag=0;
   tio_ice.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
   tio_ice.c_lflag=0;
   tio_ice.c_cc[VMIN]=0;
   tio_ice.c_cc[VTIME]=5;
   cfsetospeed(&tio_ice,B9600);            // 9600 baud
   cfsetispeed(&tio_ice,B9600);            // 9600 baud
   tty_ice = -1;

   if(have_mvat){
      memset(&tio_mvat,0,sizeof(tio_mvat));
      tio_mvat.c_iflag=0;
      tio_mvat.c_oflag=0;
      tio_mvat.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
      tio_mvat.c_lflag=0;
      tio_mvat.c_cc[VMIN]=0;
      tio_mvat.c_cc[VTIME]=5;
      cfsetospeed(&tio_mvat,B19200);            // 9600 baud
      cfsetispeed(&tio_mvat,B19200);            // 9600 baud
      tty_mvat = -1;
   }
};

vector<string> QLaser::ExchangeIce(string cmd){
   int verbose = 0;
   if(verbose) cout << "Sending: " << cmd << endl;
   cmd += "\r\n";
   tcflush(0, TCIOFLUSH);
   int n = write(tty_ice,cmd.c_str(),cmd.size());
   if(n != int(cmd.size()))
      mfe->Msg(MERROR, "ExchangeIce", "Size mismatch: %d != %d", n, cmd.size());
   // assert(n == int(cmd.size()));
   usleep(500000);
   char buf[18];
   n = read(tty_ice,&buf,17);
   buf[17] = 0;
   // cout << "Read " << n << endl;
   std::istringstream iss(&buf[2]);
   vector<string> replies;
   if(verbose) cout << "Received: ";
   do {
      string s;
      iss >> s;
      replies.push_back(s);
      if(verbose) cout << s << "+++";
   } while(iss.good());
   if(verbose) cout << endl;

   return replies;
};

string QLaser::ExchangeMvat(string cmd){
   if(!have_mvat)
      return string("No MVAT");
   cmd.insert(0,";AT:");
   cmd += "\r\n";
   tcflush(0, TCIOFLUSH);
   int n = write(tty_mvat,cmd.c_str(),cmd.size());
   if(n != int(cmd.size()))
      mfe->Msg(MERROR, "ExchangeMvat", "Size mismatch: %d != %d", n, cmd.size());
   // assert(n == int(cmd.size()));
   // cout << "Wrote " << n << endl;
   usleep(500000);
   char buf[18];
   n = read(tty_mvat,&buf,17);
   buf[n-1] = 0;
   return string(buf);
};

bool QLaser::Init(){
   bool status = InitMEMS();
   fS->RI("Frequency", &freq, true);
   fS->RI("QSDelay", &qsdelay, true);
   freq = GetFreq();
   qsdelay = GetDelay();
   fS->WI("Frequency", freq);
   fS->WI("QSDelay", qsdelay);
   fS->WI("Port", -1);
   int pump = GetPumpState();
   if(pump >= 0)
      fS->WB("Pump", bool(pump));
  
   last_on = std::chrono::system_clock::now();
   return status;
}

bool QLaser::InitMEMS(){
   bool status = false;
   mems = new MEMS;
   if(mems) status = mems->Init();
   if(status){
      mems->SwitchHV(true);
      mems->SetMidscale();
      std::cout << "MEMS initialized." << std::endl;
   }
   return status;
}

void QLaser::ReleaseMEMS(){
   if(mems) delete mems;
   mems = nullptr;
}

bool QLaser::CheckConnection(){
   return CheckConnectionIce()&&(!have_mvat || CheckConnectionMvat());
}

bool QLaser::CheckConnectionIce(){
   vector<string> rep = ExchangeIce("x");
   if(rep.size()) return (rep[0].compare("ICE450") == 0);
   else return false;
};

bool QLaser::CheckConnectionMvat(){
   string rep = ExchangeMvat("VN");
   if(rep.size()) return (rep.compare("0.11") == 0);
   else return false;
};

string QLaser::CheckInterlock(){
   std::ostringstream intlk;
   vector<string> rep;
   {
      std::ostringstream oss;
      rep = ExchangeIce("if1");
      for(unsigned int i = 1; i < rep.size(); i++){
         oss << rep[i];
      }
      size_t pos = 0;
      while(pos != string::npos){
         pos = oss.str().find('1',pos);
         switch(pos){
         case 0: intlk << "StopBtn"; break;
         case 1: intlk << (intlk.str().size()?" ":"") << "BNC"; break;
         case 2: intlk << (intlk.str().size()?" ":"") << "LasHot"; break;
         case 3: intlk << (intlk.str().size()?" ":"") << "LasOpn"; break;
         case 4: intlk << (intlk.str().size()?" ":"") << "PSOpn"; break;
         case 5: intlk << (intlk.str().size()?" ":"") << "CBusErr"; break;
         case 6: intlk << (intlk.str().size()?" ":"") << "PBusErr"; break;
         case 7: intlk << (intlk.str().size()?" ":"") << "FlashTimeout"; break;
         }
         if(pos != string::npos) pos++;
      }
   }
   {
      std::ostringstream oss;
      rep = ExchangeIce("if2");
      for(unsigned int i = 1; i < rep.size(); i++){
         oss << rep[i];
      }
      size_t pos = 0;
      while(pos != string::npos){
         pos = oss.str().find('1',pos);
         switch(pos){
         case 0: intlk << "HeaterLow"; break;
         case 1: intlk << (intlk.str().size()?" ":"") << "SimmerHot"; break;
         case 2: intlk << (intlk.str().size()?" ":"") << "H2OCold"; break;
         case 3: intlk << (intlk.str().size()?" ":"") << "H2OHot"; break;
         case 4: intlk << (intlk.str().size()?" ":"") << "H2OLow"; break;
         case 5: intlk << (intlk.str().size()?" ":"") << "FlowLow"; break;
         case 6: intlk << (intlk.str().size()?" ":"") << "TempErr"; break;
         case 7: intlk << (intlk.str().size()?" ":"") << "PwrHigh"; break;
         }
         if(pos != string::npos) pos++;
      }
   }
   {
      std::ostringstream oss;
      rep = ExchangeIce("if3");
      for(unsigned int i = 1; i < rep.size(); i++){
         oss << rep[i];
      }
      size_t pos = 0;
      while(pos != string::npos){
         pos = oss.str().find('1',pos);
         switch(pos){
         case 0: intlk << "ChrgErr"; break;
         case 1: intlk << (intlk.str().size()?" ":"") << "VHigh"; break;
         case 2: intlk << (intlk.str().size()?" ":"") << "SimrErr"; break;
         case 3: intlk << (intlk.str().size()?" ":"") << "FExtLow"; break;
         case 4: intlk << (intlk.str().size()?" ":"") << "FExtHigh"; break;
         case 5: intlk << (intlk.str().size()?" ":"") << "CapErr"; break;
         case 6: intlk << (intlk.str().size()?" ":"") << "SimrTO"; break;
         case 7: intlk << (intlk.str().size()?" ":"") << "SlvIntlk"; break;
         }
         if(pos != string::npos) pos++;
      }
   }
   {
      std::ostringstream oss;
      rep = ExchangeIce("iq");
      for(unsigned int i = 1; i < rep.size(); i++){
         oss << rep[i];
      }
      size_t pos = 0;
      while(pos != string::npos){
         pos = oss.str().find('1',pos);
         switch(pos){
         case 0: intlk << "Delay"; break;
         case 1: intlk << (intlk.str().size()?" ":"") << "H2OCold"; break;
         case 2: intlk << (intlk.str().size()?" ":"") << "QS_TO"; break;
         case 3: intlk << (intlk.str().size()?" ":"") << "Shutr"; break;
         }
         if(pos != string::npos) pos++;
      }
   }
   return intlk.str();
};

bool QLaser::Connect(){
   vector<string> dev = FindTtyUSB();
   if(dev.size() < (have_mvat?2:1)){
      mfe->Msg(MERROR, "Connect", "Not enough ttyUSB devices found.");
      return false;
   }
   bool foundIce(false), foundMvat(false);
   for(unsigned int i = 0; i < dev.size(); i++){
      if(!foundIce){
         tty_ice=open(dev[i].c_str(), O_RDWR);        // O_NONBLOCK might override VMIN and VTIME, so read() may return immediately.
         tcsetattr(tty_ice,TCSANOW,&tio_ice);
         if(tty_ice < 0){
            mfe->Msg(MERROR, "Connect", "Cannot open %s",  dev[i].c_str());
            continue;
         }
         if(CheckConnectionIce()){
            mfe->Msg(MINFO, "Connect", "Found ICE450 on %s",  dev[i].c_str());
            foundIce = true;
            continue;
         }
      }
      if(have_mvat && !foundMvat){
         tty_mvat=open(dev[i].c_str(), O_RDWR);        // O_NONBLOCK might override VMIN and VTIME, so read() may return immediately.
         tcsetattr(tty_mvat,TCSANOW,&tio_mvat);
         if(tty_mvat < 0){
            mfe->Msg(MERROR, "Connect", "Cannot open %s",  dev[i].c_str());
            continue;
         }
         if(CheckConnectionMvat()){
            mfe->Msg(MINFO, "Connect", "Found MVAT on %s",  dev[i].c_str());
            string rep = ExchangeMvat("AE 0"); // Turn off analog control
            foundMvat = true;
            continue;
         }
      }
   }
   return foundIce && (!have_mvat || foundMvat);
};

bool QLaser::Stop(){
   vector<string> rep = ExchangeIce("s");
   bool ok = rep.size();
   if(ok) {
      ok = (rep[0].compare("standby") == 0);
      mfe->Msg(MINFO, "Stop", "Laser output stopped.");
      ResetODB();
   }
   return ok;
};

bool QLaser::SetAtt(int att){
   if(!have_mvat) return true;
   char cmd[8];
   sprintf(cmd,"AP %x",att);
   string rep = ExchangeMvat(cmd);
   return (rep.compare("ok") == 0);
}

string QLaser::Status(){
   vector<string> rep = ExchangeIce("st");
   std::ostringstream oss;
   int flashstate(-1), qsstate(-1);
   if(rep[0] == "standby"){
      flashstate = 0;
      qsstate = 0;
   } else if(rep[0] == "simmer"){
      flashstate = 1;
      qsstate = 0;
   } else if(rep[0] == "fire"){
      flashstate = 2;
      qsstate = 0;
      if(rep.size() > 2){
         if(rep[2] == "qs")
            qsstate = 1;
      }
   }

   if(flashstate) last_on = std::chrono::system_clock::now();

   if(qsstate){
      mfe->TriggerAlarm("Laser ON", "Laser is firing", "Info");
   } else {
      mfe->ResetAlarm("Laser ON");
   }
   
   fV->WI("Flash", flashstate);
   fV->WI("QSwitch", qsstate);

   for(unsigned int i = 0; i < rep.size(); i++){
      if(i) oss << ' ';
      oss << rep[i];
   }
   return oss.str();
};

bool QLaser::Simmer(){
   vector<string> rep = ExchangeIce("m");
   bool ok = rep.size();
   if(ok) {
      ok = (rep[0].compare("simmer") == 0);
   }
   return ok;
}

bool QLaser::StartFlash(){
   vector<string> rep = ExchangeIce("a");
   bool ok = (rep.size() >= 2);
   if(ok)
      ok = (rep[0].compare("fire") == 0 && rep[1].compare("auto") == 0);
   if(!ok){
      std::cerr << rep.size() << std::endl;
      for(unsigned int i = 0; i < rep.size(); i++)
         std::cerr << '>' << rep[i] << '<' << std::endl;
   }
   return ok;
}

bool QLaser::StartQS(){
   vector<string> rep = ExchangeIce("cc");
   bool ok = (rep.size() >= 3);
   if(ok)
      ok = (rep[0].compare("fire") == 0 && rep[1].compare("auto") == 0 && rep[2].compare("qs") == 0);
   else
      for(unsigned int i = 0; i < rep.size(); i++)
         std::cerr << i << ": >" << rep[i] << "<" << std::endl;
   if(!ok)
      std::cerr << rep.size() << std::endl;
   if(!ok && rep.size() >= 2)
      std::cerr << rep[0] << '\t' << rep[1]<< std::endl;
   return ok;
}

bool QLaser::StopQS(){
   vector<string> rep = ExchangeIce("cs");
   bool ok = (rep.size() >= 1);
   if(ok){
      ok = (rep[0].compare("standby") == 0);
      if(!ok){
         if(rep.size() >= 2){
            ok = (rep[0].compare("fire") == 0 && rep[1].compare("auto") == 0);
            if(rep.size() == 3) ok = (rep[2].length()==0);
            else ok = false;
         } else ok = false;
      }
   }
   if(!ok)
      for(unsigned int i = 0; i < rep.size(); i++)
         std::cerr << i << ": >" << rep[i] << "<" << std::endl;
   return ok;
}

bool QLaser::SetFreq(int freq){
   std::ostringstream oss;
   oss << "d" << freq*100;
   vector<string> rep = ExchangeIce(oss.str());
   bool ok = (rep[0].compare("freq.") == 0);

   double outfreq;
   if(ok){
      outfreq = atof(rep[1].c_str());
      ok = (outfreq==freq);
   }
   return ok;
}

int QLaser::GetFreq(){
   vector<string> rep = ExchangeIce("d");
   bool ok = (rep[0].compare("freq.") == 0 && rep.size() > 1);

   int outfreq = -1;
   if(ok){
      outfreq = atof(rep[1].c_str());
   }
   return outfreq;
}

bool QLaser::SetDelay(int delay){
   std::ostringstream oss;
   oss << "w" << delay;
   vector<string> rep = ExchangeIce(oss.str());
   bool ok = (rep[0].compare("delay") == 0);

   int outdelay;
   if(ok){
      outdelay = atoi(rep[1].c_str());
      ok = (outdelay==delay);
   }
   return ok;
}

int QLaser::GetDelay(){
   vector<string> rep = ExchangeIce("w");
   bool ok = (rep[0].compare("delay") == 0 && rep.size() > 1);

   int outdelay = -1;
   if(ok){
      outdelay = atoi(rep[1].c_str());
   }
   return outdelay;
}

int QLaser::GetFLCount(){
   vector<string> rep = ExchangeIce("uf");
   if(rep.size() < 2) return -1;
   if(rep[0] != "cu" || rep[1].substr(0,2) != "LP"){
      cerr << "Bad response: " << rep[0] << " " << rep[1] << endl;
      return -2;
   }
   return std::stoi(rep[1].substr(2));
}

vector<double> QLaser::GetTemps(){
   vector<double> temps;
   vector<string> rep = ExchangeIce("t3");
   if(rep.size() >= 4){
      if(rep[0] == "T3"){
         for(unsigned int i = 1; i < 4; i++){
            temps.push_back(std::stod(rep[i])/10.);
         }
      }
   }
   return temps;
}

int QLaser::CoolantLvlOK(){
   vector<string> rep = ExchangeIce("lev");
   if(rep.size() < 2) return 2;
   return int(rep[1] == "OK");
}

double QLaser::GetFlow(){
   vector<string> rep = ExchangeIce("flow");
   if(rep.size() < 3) return -1.;
   if(rep[0] != "FLOW") return -2.;
   return std::stod(rep[1]);
}

int QLaser::GetPumpState(){
   vector<string> rep = ExchangeIce("pump");
   if(rep.size() < 3)
      return -1;
   if(rep[0].compare("PUMP") != 0)
      return -2;
   pumpstate = (rep[1].compare("ON") == 0);
   return int(pumpstate);
}

int QLaser::SetPumpState(bool state){
   char buf[32];
   sprintf(buf, "pump%d", int(state));
   vector<string> rep = ExchangeIce(buf);

   if(rep.size() < 2)
      return -1;
   if(rep[0].compare("PUMP") != 0)
      return -2;
   pumpstate = (rep[1].compare("ON") == 0);
   if(pumpstate) last_on = std::chrono::system_clock::now();
   return int(pumpstate);
}

void QLaser::GetVars(){
   fV->WI("Frequency", GetFreq());
   fV->WI("QSDelay", GetDelay());
   fV->WI("FlashlampCount", GetFLCount());
   fV->WB("CoolantLvl", bool(CoolantLvlOK()));
   fV->WD("Flow", GetFlow());
   fV->WI("PumpState", GetPumpState());
   vector<double> T = GetTemps();
   if(T.size() == 3){
      fV->WD("TCoolant", T[0]);
      fV->WD("TShg", T[1]);
      fV->WD("TCs", T[2]);  
   }
}

bool QLaser::SetPort(short i){
   if(i < -1){
      ReleaseMEMS();
      return true;
   } else {
      if(!mems){
         if(!InitMEMS()) return false;
      }
      if(i < 0){
         mems->SetMidscale();
         port = i;
         return true;
      } else if(i < (short)portX.size()){
         port = i;
         return mems->SetCoordinates(portX[i], portY[i]);
      } else {
         return false;
      }
   }
}

bool QLaser::ResetODB(bool att){
   if(!fS) return false;
   int flash, qs, atten, freq, pumpstate;
   bool pump(false);
   vector<int> portDelay;
   fS->RI("Flash", &flash, true);
   fS->RI("QSwitch", &qs, true);
   fS->RI("Attenuator", &atten, true);
   fS->RI("Frequency", &freq, true);
   fS->RI("QSDelay", &qsdelay, true);
   fS->RI("Port", &port, true);
   fS->RIA("PortX", &portX, true, 5);
   fS->RIA("PortY", &portY, true, 5);
   fS->RIA("PortQSDelay", &portDelay, true, 5);
   fS->RB("Pump", &pump, true);
   fS->RI("StandbyMinutes", &standby, true);
   vector<string> pn(5);
   fS->RSA("PortFiber", &pn, true, 5);
   fS->WI("Flash", 0);
   fS->WI("QSwitch", 0);
   if(att) fS->WI("Attenuator", 0);
   if(!fV) return false;
   fV->RI("Flash", &flash, true);
   fV->RI("QSwitch", &qs, true);
   fV->RI("Frequency", &freq, true);
   fV->RI("QSDelay", &qsdelay, true);
   double tmp;
   fV->RD("TCoolant", &tmp, true);
   fV->RD("TShg", &tmp, true);
   fV->RD("TCs", &tmp, true);
   bool tmpbool;
   fV->RB("CoolantLvl", &tmpbool, true);
   fV->RD("Flow", &tmp, true);
   fV->RI("FlashlampCount", &qsdelay, true);
   fV->RI("PumpState", &pumpstate, true);
   return true;
}

int debug=0;
char hostname[64], progname[64], eqname[64], exptname[64];


void qlcallback(INT hDB, INT hseq, INT i, void *info){
   QLaser *ice = (QLaser*)info;

   int flash, qs, atten, freq, qsdelay, port, pumpstate;
   bool pump;
   ice->fS->RI("Flash", &flash);
   ice->fS->RI("QSwitch", &qs);
   ice->fS->RI("Attenuator", &atten);
   ice->fS->RI("Frequency", &freq);
   ice->fS->RI("QSDelay", &qsdelay);
   ice->fS->RI("Port", &port);
   ice->fS->RB("Pump", &pump);
   ice->fV->RI("PumpState", &pumpstate);
   ice->fS->RI("StandbyMinutes", &ice->standby);

   if(pump != pumpstate){
      int newstate = ice->SetPumpState(pump);
      if(newstate == int(pump)){
         ice->fV->WI("PumpState", newstate);
         ice->mfe->Msg(MINFO, "qlcallback", "Waterpump turned %s", (pump?"ON":"OFF"));
      } else {
         ice->mfe->Msg(MINFO, "qlcallback", "Waterpump control failed");
      }
   }
   if(port != ice->port){       // Make sure laser is off when moving MEMS to different port
      qs = 0;
   }
   if(flash != ice->flash){
      switch(flash){
      case 0:{
         bool ok = ice->Stop();
         if(ok){
            ice->mfe->Msg(MINFO, "qlcallback", "Stopped laser");
            ice->flash = flash;
         } else {
            ice->mfe->Msg(MERROR, "qlcallback", "Couldn't stop laser");
         }
         break;
      }
      case 1:{
         bool ok = ice->Simmer();
         if(ok){
            ice->mfe->Msg(MINFO, "qlcallback", "Laser flash lamp simmer mode");
            ice->flash = flash;
         } else {
            ice->mfe->Msg(MERROR, "qlcallback", "Couldn't enter simmer mode");
         }
         break;
      }
      case 2:{
         bool ok = ice->StartFlash();
         if(ok){
            ice->mfe->Msg(MINFO, "qlcallback", "Started flashing");
            ice->flash = flash;
         } else {
            ice->mfe->Msg(MERROR, "qlcallback", "Couldn't start flashing");
         }
         break;
      }
      default: ice->mfe->Msg(MERROR, "qlcallback", "Bad flash setting %d",  flash);
      }
      ice->fS->WI("Flash", ice->flash);
   }

   if(qs != ice->qs){
      switch(qs){
      case 0:{
         bool ok = ice->StopQS();
         if(ok){
            ice->mfe->Msg(MINFO, "qlcallback", "Stopped Q-Switch");
            ice->qs = qs;
         } else {
            ice->mfe->Msg(MERROR, "qlcallback", "Couldn't stop Q-Switch");
         }
         break;
      }
      case 1:{
         bool ok = ice->StartQS();
         if(ok){
            ice->mfe->Msg(MINFO, "qlcallback", "Started Q-Switch");
            ice->qs = qs;
         } else {
            ice->mfe->Msg(MERROR, "qlcallback", "Couldn't start Q-Switch");
         }
         break;
      }
      default: ice->mfe->Msg(MERROR, "qlcallback", "Bad QS setting %d",  qs);
      }
      ice->fS->WI("QSwitch", ice->qs);
   }

   if(freq != ice->freq){
      bool ok = ice->SetFreq(freq);
      if(ok){
         ice->mfe->Msg(MINFO, "qlcallback", "Changed laser rate to %d Hz",  freq);
      } else {
         ice->mfe->Msg(MERROR, "qlcallback", "Couldn't change laser rate");
      }
      ice->freq = freq;
   }

   if(qsdelay != ice->qsdelay){
      bool ok = ice->SetDelay(qsdelay);
      if(ok){
         ice->mfe->Msg(MINFO, "qlcallback", "Changed QS delay to %d us",  qsdelay);
      } else {
         ice->mfe->Msg(MERROR, "qlcallback", "Couldn't change QS delay");
      }
      ice->qsdelay = qsdelay;
   }

   if(atten != ice->atten){
      bool ok = ice->SetAtt(atten);
      if(ok){
         ice->mfe->Msg(MINFO, "qlcallback", "Changed laser intensity (attenuator) to %d",  atten);
      } else {
         ice->mfe->Msg(MERROR, "qlcallback", "Couldn't change laser attenuator");
      }
      ice->atten = atten;
   }

   if(port != ice->port){
      ice->SetPort(port);
   }
   // std::cout << "=========================================" << std::endl;
   // std::cout << "Flash:\t" << ice->flash << std::endl;
   // std::cout << "QS:\t" << ice->qs << std::endl;
   // std::cout << "Att:\t" << ice->atten << std::endl;
   // std::cout << "Freq:\t" << ice->freq << std::endl;
   // std::cout << "=========================================" << std::endl;
}

std::string QLaser::HandleRpc(const char* cmd, const char* args)
{   
   mfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);

   if (strcmp(cmd, "toggle_pump")==0) {
      bool state;
      fS->RB("Pump", &state);
      fS->WB("Pump", !state);
      return "OK";
   } else if (strcmp(cmd, "start_flash")==0) {
      fS->WI("Flash", 2);
      return "OK";
   } else if (strcmp(cmd, "stop_flash")==0) {
      fS->WI("Flash", 0);
      return "OK";
   } else if (strcmp(cmd, "start_qs")==0) {
      fS->WI("QSwitch", 1);
      return "OK";
   } else if (strcmp(cmd, "stop_qs")==0) {
      fS->WI("QSwitch", 0);
      return "OK";
   } else {
      return "";
   }
}

///////////////////Program Constants/////////////////////////////////////

#define WITHMIDAS 1

int main(int argc, char *argv[])
{
   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);

   /* get parameters */
   /* parse command line parameters */
   for (int i = 1; i < argc; i++) {
      if (argv[i][0] == '-' && argv[i][1] == 'd')
         debug = TRUE;
      else if (argv[i][0] == '-') {
         if (i + 1 >= argc || argv[i + 1][0] == '-') goto usage;
         if (strncmp(argv[i], "-e", 2) == 0)
            strcpy(exptname, argv[++i]);
         else if (strncmp(argv[i], "-q", 2) == 0)
            strcpy(eqname, argv[++i]);
         else if (strncmp(argv[i], "-h", 2) == 0)
            strcpy(hostname, argv[++i]);
         else if (strncmp(argv[i], "-p", 2) == 0)
            strcpy(progname, argv[++i]);
      } else {
      usage:
         printf("usage: feLaser.exe -q eqname -p progname\n");
         printf("             [-h Hostname] [-e Experiment]\n\n");
         return 0;
      }
   }

   printf("tmfe will connect to midas at \"%s\" with program name \"%s\"exptname \"%s\" and equipment name \"%s\"\n"
          , hostname, progname, exptname, eqname);

#if WITHMIDAS
   TMFE* mfe = TMFE::Instance();

   TMFeError err = mfe->Connect(progname, __FILE__, hostname);
   if (err.error) {
      printf("Cannot connect to Midas, bye.\n");
      return 1;
   }

   //mfe->SetWatchdogSec(0);

   TMFeCommon *eqc = new TMFeCommon();
   eqc->EventID = 3;
   eqc->FrontendName = progname;
   eqc->LogHistory = 1;

   TMFeEquipment* eq = new TMFeEquipment(mfe, eqname, eqc);
   eq->Init();
   eq->SetStatus("Starting...", "white");

   mfe->RegisterEquipment(eq);
#endif

   // ////////////////////////Settings//////////////////////////////

   {
      QLaser ice;
      ice.mfe = mfe;
      ice.eq = eq;
      //ice.fOdb = mfe->fOdb;
      ice.fS = eq->fOdbEqSettings; // ice.fOdb->Chdir(("Equipment/" + eq->fName + "/Settings").c_str(), true);
      ice.fV = eq->fOdbEqVariables; // ice.fOdb->Chdir(("Equipment/" + eq->fName + "/Variables").c_str(), true);

      //hotlink
      HNDLE _odb_handle = 0;
      string keypath = "Equipment/" + eq->fName + "/Settings";
      int status = db_find_key(mfe->fDB, 0, keypath.c_str(), &_odb_handle);
      status = db_watch(mfe->fDB, _odb_handle, qlcallback, (void *)&ice);
      mfe->RegisterRpcHandler(&ice);
      if (status != DB_SUCCESS){
         cm_msg(MERROR,progname,"Couldn't set up db_watch: ERROR%d", status);
         return 3;
      }
      if(!ice.ResetODB(true)){
         ice.mfe->Msg(MERROR, "ResetODB", "Couldn't set ODB values for Laser to OFF");
      }

      bool connected = ice.Connect();
      if(!ice.Init()){
         ice.mfe->Msg(MERROR, progname, "Couldn't initialize MEMS communication");
         return 7;
      }
      if(connected){
         eq->SetStatus("Connected", "#00CC00");
      }
#if WITHMIDAS
      // Main loop
      while(!mfe->fShutdownRequested) {
#endif
         connected = ice.CheckConnection();
         if(!connected) connected = ice.Connect();
#if WITHMIDAS
         if(!connected){
            ice.mfe->Msg(MERROR, "Connect", "Cannot connect to Laser, bye.");
            return 1;
         }
         string intlk = ice.CheckInterlock();
         if(intlk.size()){
            string stat;
            if(!ice.pumpstate) stat = "Pump OFF - ";
            stat += "Interlock   :  ";
            stat += intlk;
            eq->SetStatus(stat.c_str(), "#FFFF00");
         } else {
            string stat("Laser Status:  ");
            string icestat = ice.Status();
            stat += icestat;
            string col = "#00CC00";
            if(icestat == "fire auto ")
               col = "#00CCAA";
            else if(icestat == "fire auto qs ")
               col = "#00FFFF";
            eq->SetStatus(stat.c_str(), col.c_str());
         }

         ice.GetVars();
         std::chrono::duration<double> seconds_off = std::chrono::system_clock::now() - ice.last_on;
         int min_off = seconds_off.count()/60;
         // cout << "Last on " << min_off << " minutes ago" << endl;
         if(ice.pumpstate && min_off >= ice.standby){
            ice.mfe->Msg(MINFO, "Standby", "Laser has been on standby for %d minutes, turning off water pump", min_off);
            ice.fS->WB("Pump", false);
         }

         // if(ice.SetAtt(13)) cout << "Attenuator set." << endl;
         // else cout << "Boing." << endl;
         for (int i=0; i<1; i++) {
            mfe->PollMidas(1000);
            if (mfe->fShutdownRequested)
               break;
         }
         if (mfe->fShutdownRequested){
            break;
         }
      }
#endif
   }
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
