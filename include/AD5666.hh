#include <bcm2835.h>
#include <cstdint>
#include "GPIOInterface.hh"
#include "SpiInterface.hh"
//#include <phidget22.h>
//#include "VoltageInput.hh"

#define AD_CMD_WRITE_REG 0x0
#define AD_CMD_UPD_REG   0x1
#define AD_CMD_POWUPDN   0x4
#define AD_CMD_RESET     0x7
#define AD_CMD_DCEN_REF  0x8

#define AD_ALL_DACS      0xf

class AD5666
{
public:
  AD5666(uint8_t ch_sel = BCM2835_SPI_CS0, uint16_t div = BCM2835_SPI_CLOCK_DIVIDER_256): spi(true, ch_sel, 2, div){}
  bool Init();
  bool SendCommand(uint8_t cmd, uint8_t addr, uint16_t data, uint8_t special = 0); //PW - cmd = Table7, addr = Table 8, data = D15 - D0. So where does voltage get set? - MISO/MOSI, special are written in another file
  bool SetIntRefDaisy(bool int_ref = true, bool daisy = false);
  //bool SetIntRefDaisy(bool int_ref = true, bool daisy = true); //PW - need to write daisy output
  bool SetDACReg(uint8_t addr, uint16_t data);
  bool PulseLDAC();
  bool Apply(){ return PulseLDAC(); }
  void ExchangeBytes(const char *send_data, char *data_out, uint32_t nbytes);
  template <class T> void Exchange(const T &data, T &dataout)
  {
    ExchangeBytes((char*)&data, (char*)&dataout, sizeof(data));
  }
  //~VoltageInput(); //need to initialize voltage input

  RaspberryPi::GPIOInterface gpio;

private:
  RaspberryPi::SpiInterface spi;
};
