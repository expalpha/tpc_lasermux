#ifndef PD_HH
#define PD_HH

#include "VoltageInput.hh"
#include "TektronixScope.hh"

class PD
/// Wrapper class to read out Photo diode via scope or phidget
{
public:
  enum accesstype { phidget, scope_tek };
  /*! \brief Constructor
   *
   * @param t read out PD via phidget or scope
   * @param chan channel number
   */
  PD(accesstype t, unsigned short chan = 0): acc(t), channel(chan){};
  bool Init(int numAv = 4, double timescale = 500.e-6); ///< Initialize **REQUIRED**
  double GetV(); ///< Read back voltage or pulseheight
  void SetVerbose(int verbose){ scope.verbose = verbose; };
  accesstype acc;
private:
  unsigned short channel;
  TektronixScope scope;
  PhidgetPP::VoltageInput *v_in;
  double maxV = 5.;
  double trigLev = 0.1;
};

#endif
