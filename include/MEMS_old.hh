#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fstream>

#include "AD5666.hh"

#define DAC_ALL 15

class MEMS_old
{
public:
  bool Init();
  bool SetMidscale();
  int CheckSafe(double xoffset, double yoffset);
  bool Ramp(double xoffset, double yoffset, double stepsize=64, double timedelay=10000);
  int SetCoordinates(double stepsize, double timedelay);
  enum scantype { coarse, fine };
  std::pair<double, double> Scan(double xoffset, double yoffset, int port, scantype type = fine);
  bool FindPort();
  double GetScopeMax();
  std::pair<double, double> FindPort(int port);
  // bool FindCore(double xoffset, double yoffset, int port);
  void SwitchPD(bool state);
  void ShutDown();
private:
  AD5666 dac;
  double xcurrent = 0;
  double ycurrent = 0;
  std::fstream scope;
  const uint32_t readback = 9 <<24;
  void Step(double x, double y);
};
//////////////////////////////////////////////////////////////
