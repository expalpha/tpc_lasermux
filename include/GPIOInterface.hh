#ifndef GPIOINT_HH
#define GPIOINT_HH
#include <bcm2835.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <cstdint>
#include <map>

namespace RaspberryPi{
extern int bcm_connections;
class GPIOInterface {
/*! \brief Class for accessing raspberry pi GPIO pins
 */
public:
    bool Init();
    ~GPIOInterface()
    {
        std::cout << "~GPIOInterface()" << std::endl;
        if(isOpen){
            if(--bcm_connections == 0){
                std::cout << "Closing bcm" << std::endl;
                bcm2835_close();
            }
        }
    }

    bool ReadPin(uint8_t pin);
    bool SetPin(const uint8_t pin, const bool val);
    bool SetPUD(const uint8_t pin, const char pud = 'n');
    void GpClockOn();

    bool isOpen = false;
private:
    bool MapPin(const uint8_t pin, uint8_t &pout);
    const std::map<uint8_t, uint8_t> pimap = {
        { 3, RPI_V2_GPIO_P1_03 },
        { 5, RPI_V2_GPIO_P1_05 },
        { 7, RPI_V2_GPIO_P1_07 },
        { 8, RPI_V2_GPIO_P1_08 },
        { 10, RPI_V2_GPIO_P1_10 },
        { 11, RPI_V2_GPIO_P1_11 },
        { 12, RPI_V2_GPIO_P1_12 },
        { 13, RPI_V2_GPIO_P1_13 },
        { 15, RPI_V2_GPIO_P1_15 },
        { 16, RPI_V2_GPIO_P1_16 },
        { 18, RPI_V2_GPIO_P1_18 },
        { 19, RPI_V2_GPIO_P1_19 },
        { 21, RPI_V2_GPIO_P1_21 },
        { 22, RPI_V2_GPIO_P1_22 },
        { 23, RPI_V2_GPIO_P1_23 },
        { 24, RPI_V2_GPIO_P1_24 },
        { 26, RPI_V2_GPIO_P1_26 },
        { 29, RPI_V2_GPIO_P1_29 },
        { 31, RPI_V2_GPIO_P1_31 },
        { 32, RPI_V2_GPIO_P1_32 },
        { 33, RPI_V2_GPIO_P1_33 },
        { 35, RPI_V2_GPIO_P1_35 },
        { 36, RPI_V2_GPIO_P1_36 },
        { 37, RPI_V2_GPIO_P1_37 },
        { 38, RPI_V2_GPIO_P1_38 },
        { 40, RPI_V2_GPIO_P1_40 }
    };
};
}
#endif
