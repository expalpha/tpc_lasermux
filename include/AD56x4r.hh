#ifndef AD56X4R_HH
#define AD56X4R_HH

#include <bcm2835.h>
#include <cstdint>
#include "SpiInterface.hh"

#define AD_CMD_WRITE_REG 0x0
#define AD_CMD_UPD_REG   0x1
#define AD_CMD_WRITE_REG_UPD_ALL 0x2
#define AD_CMD_WRITE_REG_UPD_REG 0x3
#define AD_CMD_POWUPDN   0x4
#define AD_CMD_RESET     0x5
#define AD_CMD_LDAC     0x6
#define AD_CMD_INT_REF  0x7

#define AD_ALL_DACS      0x7

class AD56x4r
/*! \brief Class for communication with Analog Devices AD56x4R DAC chips
 *
 * Uses raspberry pi SPI interface for communication
 */
{
public:
    /*! \brief Constructor
    *
    * @param ch_sel Channel select, 0 or 1
    * @param div Clock divider
    */
    AD56x4r(uint8_t ch_sel = BCM2835_SPI_CS0, uint16_t div = BCM2835_SPI_CLOCK_DIVIDER_256): spi(true, ch_sel, 2, div){}

    /*! \brief Initialize. **MUST BE CALLED**
    *
    * Required to be called before use
    * @return *true* if SPI interface was opened successfully
    */
    bool Init();

    /*! \brief Generate data word and send
    *
    * Assembles SPI message and sends it to device.
    * @param cmd 3 bit command
    * @param addr DAC channel address
    * @param data new value
    * @param special extra info for certain commands
    * @return *true* if SPI device is open
    */
    bool SendCommand(uint8_t cmd, uint8_t addr, uint16_t data, uint8_t special = 0); //PW - cmd = Table7, addr = Table 8, data = D15 - D0. So where does voltage get set? - MISO/MOSI, special are written in another file
    bool SetIntRef(bool int_ref = true); ///< Switch DAC internal reference mode

    /*! \brief Write to a given DAC channel register
    *
    * Assembles SPI message and sends it to device.
    * @param addr DAC channel address
    * @param data new value
    * @param apply set DAC voltages to register values
    * @return *true* if SPI device is open
    */
    bool SetDACReg(uint8_t addr, uint16_t data, bool apply = false);

    /*! \brief Switch "Instant Write" mode
    *
    * Determine whether writing to DAC register automatically sets voltage or not
    * @param instantWrite *true* to set voltages instantly, *false* to only set on apply command
    * @return *true* if SPI device is open
    */
    bool SetLDAC(bool instantWrite = false);

    bool Reset();   ///< Reset DAC to default values
    bool Enable(uint8_t channelmask); ///< Enable channels by mask

private:
    RaspberryPi::SpiInterface spi;
};

#endif
