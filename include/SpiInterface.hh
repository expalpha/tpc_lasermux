#ifndef SPI_INTF_HH
#define SPI_INTF_HH
// spitest.cc
//
// Example program for bcm2835 library
// Shows how to interface with SPI to transfer a byte to and from an SPI device
//
// After installing bcm2835, you can build this
// with something like:
// gcc -o spi spi.c -l bcm2835
// sudo ./spi
//
// Or you can test it before installing with:
// gcc -o spi -I ../../src ../../src/bcm2835.c spi.c
// sudo ./spi
//
// Author: Mike McCauley
// Copyright (C) 2012 Mike McCauley
// $Id: RF22.h,v 1.21 2012/05/30 01:51:25 mikem Exp $

#include <bcm2835.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

namespace RaspberryPi{
extern int bcm_connections;
class SpiInterface {
/*! \brief Class for accessing raspberry pi SPI interface
 */
public:
/*! \brief Constructor
*
* @param bigEndian Endianness of SPI device
* @param ch_sel CS or CE or SYNC or whatever channel selection pin
* @param mode SPI mode (0-3), see wikipedia
* @param div SPI clock divider to select bitrate
*/
  SpiInterface(bool bigEndian = false, uint8_t ch_sel = BCM2835_SPI_CS0,
	       uint8_t mode = BCM2835_SPI_MODE0, uint16_t div = BCM2835_SPI_CLOCK_DIVIDER_65536):
    bigEnd(bigEndian),
    cs(ch_sel),
    spimode(mode),
    clock_div(div)
  {}

/*! \brief Initialize SPI interface **MUST BE CALLED**
*
* Initializes SPI device with selected settings
* @return *true* if successful
*/
  bool Init();
  ~SpiInterface()
  {
    std::cout << "~SpiInterface()" << std::endl;
    if(isOpen){
      bcm2835_spi_end();
      if(--bcm_connections == 0){
          std::cout << "Closing bcm" << std::endl;
          bcm2835_close();
      }
    }
  }

  /*! \brief Send and receive a number of bytes
  *
  * @param send_data data to send
  * @param rbuf buffer to receive data into
  * @param nbytes length of message
  */
  void ExchangeBytes(const char  *send_data, char *rbuf, uint32_t nbytes);

  /*! \brief Send and receive variable
  *
  * @param data data to send
  * @param data_out variable to receive data
  */
  template <class T> void Exchange(const T &data, T &dataout)
  {
    ExchangeBytes((const char*)&data, (char*)&dataout, sizeof(data));
  }

  /*! \brief Send a number of bytes
  *
  * @param send_data data to send
  * @param nbytes length of message
  */
  void SendBytes(const char *send_data, uint32_t nbytes);

  /*! \brief Send variable
  *
  * @param data data to send
  */
  template <class T> void Send(const T &data)
  {
    SendBytes((const char*)&data, sizeof(data));
  }

  void SetClockDiv(uint16_t div);
  void SetSPIMode(uint8_t mode);
  void SetCS(uint8_t ch_sel);
  bool isOpen = false;  ///< *true* if SPI interface is open.

private:
  bool bigEnd = false;
  uint8_t cs = BCM2835_SPI_CS0; // == 0
  uint8_t spimode = BCM2835_SPI_MODE0; // == 0
  uint16_t clock_div = BCM2835_SPI_CLOCK_DIVIDER_65536; // == 0
};
}
#endif
