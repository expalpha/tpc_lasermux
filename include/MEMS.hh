#ifndef MEMS_HH
#define MEMS_HH
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdint>

#include "AD56x4r.hh"
#include "GPIOInterface.hh"

class MEMS
/*! \brief Class for controlling the mirrorcletech Digital MEMS controller
 *
 * Controls SPI communication with onboard DAC and switches the EN pin.
 * Also switches another GPIO pin, used to turn on the separate photodiode
 */
{
public:
    ~MEMS(){ SwitchPD(false); }
    void SetMidscale();///< Set all channels to midscale for differential driving

    /*! \brief Set mirror position in DAC units
    *
    * Drives the mirror to given differential coordinates from midscale.
    * Checks settings for safety.
    * @param delta_x x position in DAC units limited by maxdiff
    * @param delta_y y position in DAC units limited by maxdiff
    * @return *true* if settings within safe limits, *does not guarantee settings were applied*
    */
    bool SetCoordinates(int32_t delta_x, int32_t delta_y);

    /*! \brief Slowly ramp to desired position
    * 
    * Less needed if on-board filter is used.
    * Will ramp up to maxdiff if requested further.
    * @param delta_x x position in DAC units limited by maxdiff
    * @param delta_y y position in DAC units limited by maxdiff
    * @param stepsize DAC units to move per step
    * @param timedelay microseconds to wait between steps
    * @return *true* if postioin was within limits, doesn't check actual MEMS movement
    */
    bool Ramp(int32_t delta_x, int32_t delta_y, uint16_t stepsize=64, uint16_t timedelay=10000);

    /*! \brief Initialize MEMS **REQUIRED**
    *
    * Initialize communication with the MEMS, make sure to check return value.
    * @return *true* if SPI and GPIO devices work, no feedback from DAC
    */
    bool Init();

    /*! \brief Enable MEMS driver
    *
    * MEMS driver should be enabled at start of program and disabled at the end,
    * unless MEMS is meant to stay in position.
    * @param state *true* to enable, *false* to disable.
    */
    void SwitchHV(bool state);

    /*! \brief Enable Photodiode power
    *
    * Sets GPIO pin to power PD up/down.
    * Not strictly part of the MEMS, but easier for our operation.
    * @param state *true* to enable, *false* to disable.
    */
    void SwitchPD(bool state);

    /*! \brief Shutdown MEMS
    *
    * Ramp down all voltages and turn off
    */
    void ShutDown();
    const uint16_t maxdiff = 150*0xFFFF/200; ///< Max. V difference from MEMS datasheet
    int verbose = 0;
private:
    AD56x4r dac;    ///< DAC chip on controller
    RaspberryPi::GPIOInterface gpio; ///< Access to GPIO pins

    /*! \brief Zero-point for MEMS operation
    *
    * Needed to allow bi-directional operation
    * 90V is recommended bias voltage for our MEMS
    */
    const uint16_t midscale = 90*0xFFFF/200;
    int32_t x, y; ///< Current position
};
//////////////////////////////////////////////////////////////
#endif
