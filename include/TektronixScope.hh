#ifndef TEKTR_HH
#define TEKTR_HH

#include <fstream>

class TektronixScope
/*! \brief Class to communicate with a Tektronix oscilloscope via USBTMC
*
* Only rudimentary, does not allow access to most functions.
* mostly meant to read back pulse height
*/
{
private:
   const std::string devID = "TEKTRONIX";
   std::fstream stream;
public:
   int verbose = 1;
   double GetV(); ///< Read peak voltage
   bool CheckConnection(); ///< Confirm scope is connected and responsive

   /*! \brief Set settings for data acquisition
   *
   * Only some settings are accessible
   * @param channel Channel number to read out, also trigger channel atm.
   * @param timescale Seconds per division
   * @param maxV Full y scale
   * @param trigLev Trigger threshold
   * @return *true* if settings were applied successfully
   */
  bool SetSettings(unsigned short channel = 1, double timescale = 500.e-6, double maxV = 5., double trigLev = 0.1, int numAv = 4); //maxV=0.2
   bool Connect(); ///< Open connection to scope and check response
   ~TektronixScope();
};

#endif
