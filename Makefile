all:
	@echo Makefile no longer supported, just remains for archival purposes for now.
	@echo Please use cmake instead, i.e.:
	@echo mkdir build && cd build
	@echo cmake ..
	@echo make

runMEMS: test/runMEMS.cc SpiInterface.o GPIOInterface.o AD5666.o MEMS.o
	g++ -g -std=c++11 -o $@ $< SpiInterface.o GPIOInterface.o AD5666.o MEMS.o -Iinclude -I$(HOME)/phidgetpp/include -I/usr/include/python3.7m -I/usr/local/lib/python3.7m/site-packages/numpy/core/include -L$(HOME)/phidgetpp/build/ -lphidget22 -lphidgetpp -lbcm2835 -lcap -lpython3.7m
	sudo setcap cap_sys_rawio+ep $@

MEMS2_test: test/MEMS2_test.cc SpiInterface.o GPIOInterface.o AD56x4r.o MEMS2.o
	g++ -g -std=c++11 -o $@ $< SpiInterface.o GPIOInterface.o AD56x4r.o MEMS2.o -Iinclude -lbcm2835
	sudo setcap cap_sys_rawio+ep $@

PWtest: test/PWtest.cc SpiInterface.o GPIOInterface.o AD5666.o
	g++ -g -std=c++11 -o $@ $< SpiInterface.o GPIOInterface.o AD5666.o -Iinclude -I$(HOME)/phidgetpp/include -I/usr/include/python3.7m -I/usr/local/lib/python3.7m/site-packages/numpy/core/include -L$(HOME)/phidgetpp/build/ -lphidget22 -lphidgetpp -lbcm2835 -lcap -lpython3.7m
	sudo setcap cap_sys_rawio+ep $@

fulltest: test/fulltest.cc SpiInterface.o GPIOInterface.o
	g++ -g -o $@ $< -Iinclude -lbcm2835 -lcap SpiInterface.o GPIOInterface.o
	sudo setcap cap_sys_rawio+ep $@

spitest: test/spitest.cc SpiInterface.o
	g++ -g -o $@ $< -Iinclude -lbcm2835 -lcap SpiInterface.o
	sudo setcap cap_sys_rawio+ep $@

gpiotest: test/gpiotest.cc GPIOInterface.o
	g++ -g -o $@ $< -Iinclude -lbcm2835 -lcap GPIOInterface.o -std=c++11
	sudo setcap cap_sys_rawio+ep $@

dactest: test/dactest.cc SpiInterface.o AD5666.o GPIOInterface.o
	g++ -g -o $@ $< -Iinclude -lbcm2835 -lcap SpiInterface.o AD5666.o GPIOInterface.o
	sudo setcap cap_sys_rawio+ep $@

MEMS.o: src/MEMS.cc include/MEMS.hh
	g++ -g -c $< -Iinclude -I$(HOME)/phidgetpp/include -I/usr/include/python3.7m -I/usr/local/lib/python3.7m/site-packages/numpy/core/include

%.o: src/%.cc
	g++ -c -o $@ -Iinclude $<

clean:
	rm -f *.o fulltest spitest gpiotest dactest PWtest runMEMS MEMS
