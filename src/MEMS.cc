#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>

#include "MEMS.hh"

// Physical pin numbers on raspberry pi 3b+
#define PDPIN 29    // PD power supply enable
#define HVPIN 31    // MEMS driver "EN"

///////////////////////////////////////////////////////////////////////////////
int RaspberryPi::bcm_connections = 0;

///////////////// Set voltages to midscale ////////////
void MEMS::SetMidscale(){
    if(verbose > 0) std::cout << "midscale = 0x" << std::hex << midscale << std::endl;
    dac.SetDACReg(AD_ALL_DACS,midscale, true); //set all voltages to midscale (1.25V)
    x = 0;
    y = 0;
}

//////////////// Set input voltages ///////////////////

bool MEMS::SetCoordinates(int32_t delta_x, int32_t delta_y){
    if(abs(delta_x) > maxdiff || abs(delta_y) > maxdiff){
      std::cerr << "Unsafe settings: " << std::dec << delta_x << ", " << delta_y << " > " << maxdiff << std::endl;
        return false;
    }
    // std::cout << "X- = " << uint16_t(midscale - delta_x/2) << std::endl;
    // std::cout << "X+ = " << uint16_t(midscale + delta_x/2) << std::endl;
    // std::cout << "Y- = " << uint16_t(midscale - delta_y/2) << std::endl;
    // std::cout << "Y+ = " << uint16_t(midscale + delta_y/2) << std::endl;

    x = delta_x;
    y = delta_y;
    if(verbose > 1) std::cout << std::dec << x << '\t' << y << std::endl;
    dac.SetDACReg(0, midscale + delta_x/2); //Set x
    dac.SetDACReg(1, midscale - delta_x/2); //Set x
    dac.SetDACReg(2, midscale + delta_y/2); //Set y
    dac.SetDACReg(3, midscale - delta_y/2, true); //Set y
    return true;
}

//////////////// Scanning  ///////////////////

bool MEMS::Init(){
    //Initialize MEMS
    bool success = dac.Init();
    if(success){
        success = gpio.Init();
        dac.SetIntRef(true);
        dac.Enable(0xF);
    }
    return success;
}


bool MEMS::Ramp(int32_t delta_x, int32_t delta_y, uint16_t stepsize, uint16_t timedelay){
    //moves mirror to initial start position in small, user defined increments. Default settings are stepsize = 32, timedelay = 10000

    bool success = true;
    int32_t dx = delta_x-x;
    int32_t dy = delta_y-y;

    int32_t stepx = stepsize;
    int32_t stepy = stepsize;

    if(dx < 0) stepx = -stepsize;
    if(dy < 0) stepy = -stepsize;

    if(verbose) std::cout << "Ramping to:  " << delta_x << ", " << delta_y << std::endl;
    // std::cout << "Steps: " << stepx << ", " << stepy << std::endl;
    while(abs(dx) > 0 || abs(dy) > 0){
        usleep(timedelay);
        dx = delta_x-x;
        dy = delta_y-y;
        if(abs(dx) < stepsize){
            stepx = dx;
            // std::cout << "new stepx = " << dx << std::endl;
        }
        if(abs(dy) < stepsize){
            stepy = dy;
            // std::cout << "new stepy = " << dy << std::endl;
        }
        success &= SetCoordinates(x+stepx, y+stepy);
        dx = delta_x-x;
        dy = delta_y-y;
        if(!success) break;
    }
    return success;
}

void MEMS::SwitchPD(bool state){
    gpio.SetPin(PDPIN, state);
}

void MEMS::SwitchHV(bool state){
    gpio.SetPin(HVPIN, state);
}

void MEMS::ShutDown(){
    Ramp(0,0,1024,100);
    dac.SetDACReg(AD_ALL_DACS,0, true); //set all voltages to zero
    SwitchPD(false);
    SwitchHV(false);
}
