#include <iostream>
#include <iomanip>
#include "AD56x4r.hh"

bool AD56x4r::SendCommand(uint8_t cmd, uint8_t addr, uint16_t data, uint8_t special)
{
    if(!spi.isOpen) return false;
    uint32_t message = 0;
    message |= ((cmd & 0x7) << 19);
    message |= ((addr & 0x7) << 16);
    message |= data;//(data << 4);
    message |= (special & 0xF);
    // message = 0xF96655AA; // test pattern to figure out addressing
    // std::cout << "Sending: " << std::hex << message << std::endl;
    char* daddr = (char*)&message;
    spi.SendBytes(daddr, 3);
    return true;
}

bool AD56x4r::SetIntRef(bool int_ref)
{
  uint8_t special = 0;
  if(int_ref) special = 1;
  return SendCommand(AD_CMD_INT_REF, 0, 0, special);
}

bool AD56x4r::SetDACReg(uint8_t addr, uint16_t data, bool apply)
{
  uint8_t cmd = AD_CMD_WRITE_REG;
  if(apply) cmd = AD_CMD_WRITE_REG_UPD_ALL;
  return SendCommand(cmd, addr, data);
}

bool AD56x4r::SetLDAC(bool instantWrite)
{
  uint8_t special = 0;
  if(instantWrite) special = 0xf;
  return SendCommand(AD_CMD_LDAC, 0, 0, special);
}

bool AD56x4r::Init()
{
  bool success = spi.Init();
  if(success) return Reset();
  return success;
}

bool AD56x4r::Reset()
{
  return SendCommand(AD_CMD_RESET, 0, 0, 1);
  // return SendCommand(0x7, 0x5, 0xAA55);
}
// bool AD56x4r::PulseLDAC()
// {
//   bool success = gpio.SetPin(15, false);
//   success != gpio.SetPin(15, true);
//   return success;
// }

bool AD56x4r::Enable(uint8_t channel)
{
  return SendCommand(AD_CMD_POWUPDN, 0, 0, channel);
}

