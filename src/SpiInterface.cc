#include "SpiInterface.hh"

using RaspberryPi::SpiInterface;

bool SpiInterface::Init()
{
  isOpen = bcm2835_init();
  if(!isOpen)
    {
      printf("bcm2835_init failed. Are you running as root??\n");
      return isOpen;
    }

  isOpen = bcm2835_spi_begin();
  if(!isOpen)
    {
      printf("bcm2835_spi_begin failed. Are you running as root??\n");
      return isOpen;
    }
  bcm_connections++;
  bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default *BIT* order, doesn't affect endianness
  bcm2835_spi_setDataMode(spimode);                                // The default
  bcm2835_spi_setClockDivider(clock_div);                       // The default
  bcm2835_spi_chipSelect(cs);                                   // The default
  bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // the default
  return isOpen;
}

//uint8_t SpiInterface::ExchangeByte(uint8_t send_data)
//{
//   return bcm2835_spi_transfer(send_data);
//}

void SpiInterface::ExchangeBytes(const char *send_data, char *data_out, uint32_t nbytes)
{
  char tbuf[nbytes];
  if(bigEnd){
    char rbuf[nbytes];
    for(unsigned int i = 0; i < nbytes; i++){
      tbuf[i] = send_data[nbytes-i-1];
    }
    bcm2835_spi_transfernb(tbuf, rbuf, nbytes);
    for(unsigned int j = 0; j < nbytes; j++){
      data_out[j] = rbuf[nbytes-j-1];
    }
  } else {
    for(unsigned int i = 0; i < nbytes; i++){
      tbuf[i] = send_data[i];
    }
    bcm2835_spi_transfernb(tbuf, data_out, nbytes);
  }
}

void SpiInterface::SendBytes(const char *send_data, uint32_t nbytes)
{
  if(bigEnd){
    char buf[nbytes];
    for(unsigned int i = 0; i < nbytes; i++){
      buf[i] = send_data[nbytes-i-1];
    }
    bcm2835_spi_writenb(buf, nbytes);
  } else {
    bcm2835_spi_writenb(send_data, nbytes);
  }
}

void SpiInterface::SetClockDiv(uint16_t div)
{
  clock_div = div;
  bcm2835_spi_setClockDivider(clock_div);
}

void SpiInterface::SetSPIMode(uint8_t mode)
{
  spimode = mode;
  bcm2835_spi_setDataMode(spimode);
}

void SpiInterface::SetCS(uint8_t ch_sel)
{
  cs = ch_sel;
  bcm2835_spi_chipSelect(cs);
}

// template <class T> void SpiInterface::Send(const T &data)
// {
//     SendBytes((const char*)&data, sizeof(data));
// }
