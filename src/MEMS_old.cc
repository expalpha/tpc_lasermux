////////////////////////////////
////// Written by Pooja W //////
////// Aug 3, 2021        //////
////////////////////////////////


#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <phidget22.h> //no longer applicable
#include "VoltageInput.hh" //used with phidget - no longer applicable
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>
// #include "matplotlibcpp.h"
#include "MEMS_old.hh"

#define DAC_ALL 15
#define MIDSCALE 0x3fff //16383
//#define STEPSIZE 32
#define TIMEDELAY 1000

using std::cout;
using std::endl;
///////////////////////////////////////////////////////////////////////////////

////////////////  Initialize MEMS_old ////////////////
bool MEMS_old::Init(){
  bool success = dac.Init();
  cout<<"DAC Initialized"<<endl;
  bool success2 = dac.PulseLDAC();

  if(success){
    dac.SetIntRefDaisy(true,true);
  }
  if(success2){
      cout<<"LDAC works"<<endl;
  }
  scope.open("/dev/usbtmc0");
  success = scope.is_open();
  if(success){
    scope << "MEASU:IMMED:SOURCE:CH1" << std::endl;
    scope << "MEASU:IMMED:TYPE:MAXI" << std::endl;
    // scope << "MEASU:SOURCE 1" << std::endl;
  }
  return success;
}

//////////////// Set voltages to midscale ////////////////
bool MEMS_old::SetMidscale(){
  dac.SetDACReg(DAC_ALL,MIDSCALE); //set all voltages to midscale (1.25V)
  dac.Apply();
  return 0;
}

//////////////// Check safe range  ////////////////
int MEMS_old::CheckSafe(double xoffset, double yoffset){
  double low_range = -1*MIDSCALE; //Max range of MEMS_old
  double high_range = MIDSCALE;
  if ( xoffset < low_range || xoffset > high_range) {
    std::cerr<<"delta x is outside of safe range."<<std::endl;
    std::cout<<"Delta x should be between "<<low_range<<" and "<<high_range<<std::endl;
    std::cin>>xoffset;
    return xoffset;
  }    
  if ( yoffset < low_range || yoffset > high_range) {
    std::cerr<<"delta y is outside of safe range."<<std::endl;
    std::cout<<"Delta y should be between "<<low_range<<" and "<<high_range<<std::endl;
    std::cin>>yoffset;
    return yoffset;
  }
  return 0;
}

void MEMS_old::Step(double x, double y){
  if(x != xcurrent) dac.SetDACReg(1, x+MIDSCALE);
  if(y != ycurrent) dac.SetDACReg(2, y+MIDSCALE);
  xcurrent = x;
  ycurrent = y;
  
  usleep(TIMEDELAY);
}

bool MEMS_old::Ramp(double xoffset, double yoffset, double stepsize, double timedelay){
  //moves mirror to initial start position in small, user defined increments. Default settings are stepsize = 32, timedelay = 10000
  // FIXME: reduce duplication and maybe speed things up by going diagonal?
  // I.e. move x and y together until one has reached its target, then only move the other.
  double xstepsize = stepsize;
  double ystepsize = stepsize;
  if (xoffset < xcurrent){
    xstepsize *= -1;
  }
  if (yoffset < ycurrent){
    ystepsize *= -1;
  }
  // cout << "Ramp to " << xoffset << ", " << yoffset << endl;
  while(ycurrent != yoffset || xcurrent != xoffset){
    double x = xcurrent;
    double y = ycurrent;
    if(abs(xcurrent-xoffset)<stepsize)
      xstepsize = xoffset-xcurrent;
    if(abs(ycurrent-yoffset)<stepsize)
      ystepsize = yoffset-ycurrent;
    if(xcurrent != xoffset)
      x += xstepsize;
    if(ycurrent != yoffset)
      y += ystepsize;
    Step(x,y);
  }
  return 0;
}
 
//////////////// Move to x and y position ////////////////
int MEMS_old::SetCoordinates(double stepsize, double timedelay){
  double xoffset = 0;
  double yoffset = 0;
  std::cout << "x offset is : (a negative value goes right, a positive value goes left)" << std::endl;
  std::cin>>xoffset;
  std::cout << "y offset is : (a negative value goes up, a positive value goes down)" << std::endl;
  std::cin>>yoffset;
  CheckSafe(xoffset,yoffset);
  // double x_pos_init = MIDSCALE + xoffset;
  // double y_pos_init = MIDSCALE + yoffset;
  Ramp(xoffset,yoffset,stepsize,timedelay);
  return 0;			// FIXME: return something useful?
}


//////////////// Scanning  ///////////////////
//Scans a predetermined area
std::pair<double, double> MEMS_old::Scan(double xoffset, double yoffset, int port, scantype type){ // FIXME: ideally file IO should not be done by MEMS_old class, or if it is, file name should be an argument given by main program.

  std::pair<double, double> coords;
  coords.first = xoffset;
  coords.second = yoffset;
  int stepsize = 32*4;
  int max_x = 512*2;
  int max_y = 512*2;
  //Label the output file
  std::string filename = "Port_" + std::to_string(port);
  if (type == fine){
    stepsize = 32;
    max_x = 512;
    max_y = 256;
    std::cout << "Fine scan" << std::endl;
    filename += "_fine";
  } else {
    std::cout << "Coarse scan" << std::endl;
  }
  filename += ".txt"; //+ "_" +DateTime.Now.ToString("MM-dd-yyyy") +

  std::ofstream outputfile;
  outputfile.open(filename);
  if (!outputfile)
    {
      std::cout << "Error opening file " << filename << std::endl;
      return std::pair<double,double>(-999999.,-999999.);
    }
  outputfile << "#x position" << '\t' <<  "y position" << '\t' << " Peak " << std::endl;

  cout << "Scanning " << xoffset-max_x << ", " << yoffset-max_y << " to " << xoffset+max_x << ", " << yoffset+max_y << endl;
  cout << "Going to expected position." << endl;
  Ramp(xoffset+128, yoffset, stepsize, TIMEDELAY);
  usleep(100000);
  Ramp(xoffset, yoffset, stepsize, TIMEDELAY);
  usleep(200000);
  cout << "Reading: " << GetScopeMax() << endl;

  //Set up initial values to detemine area of scan (max_x, max_y) and initial starting position
  double x_pos_init = max_x + xoffset;
  double y_pos_init = max_y + yoffset;
  Ramp(x_pos_init, y_pos_init, stepsize, TIMEDELAY);
  usleep(200000);
  //What we expect the average max values to be
  double maxPeak = 0;
  
  //Begin scan
  std::cout << "Beginning scan: xsteps: " << 2*max_x/stepsize << ", ysteps: " << 2*max_y/stepsize << std::endl;
  for (int j = 0; j <= 2*max_y/stepsize; j++){
    double y_pos = y_pos_init - stepsize*(j);
    for (int i = 0; i <= 2*max_x/stepsize; i++){
      // Positioning is direction dependent, this doesn't work as well as it should:
      // if(i > 0){
      // 	if(j%2) x_pos += stepsize;
      // 	else x_pos -= stepsize;
      // }
      double x_pos = x_pos_init - stepsize*(i);
      if(i==0){
	  Ramp(x_pos+128, y_pos, stepsize, TIMEDELAY);
	  Ramp(x_pos, y_pos, stepsize, TIMEDELAY);
	  usleep(200000);
      }
      assert(x_pos <= x_pos_init);
      assert(x_pos >= x_pos_init-2*max_x);
      // dac.SetDACReg(1,x_pos);
      // dac.Apply();
      // usleep(TIMEDELAY);
      Step(x_pos, y_pos);
      cout << "step " << i << ", " << j << " -> x " << x_pos << " y " << y_pos <<endl;
      usleep(200000);
      double peak;
      for(int i = 0; i < 3; i++){
	peak = GetScopeMax();
	if(peak > 1e-3 && peak < 100.){
	  cout << "peak " << peak << endl;
	  outputfile << x_pos << '\t' << y_pos << '\t' << peak<<std::endl;;
	  break;
	} else {
	  peak = 0.;
	}
      }
      if (peak > maxPeak){
	maxPeak = peak;
	coords.first = x_pos;
	coords.second = y_pos;
      }
    }
  }
  outputfile << "# peak: " << coords.first << "\t" << coords.second <<endl;
  outputfile.close();

  if(maxPeak > 0){
    cout<<"Fibre core found for port "<<port<< " at (" << coords.first << ", " << coords.second << ") output saved to "<<filename<<endl;
  } else {
    coords.first = -999999.;
    coords.second = -999999.;
  }
  return coords;
}

bool MEMS_old::FindPort(){

  // FIXME: user input should be handled in main(), this function should take port number as an argument..
  /* Get user input on which port they are looking for */
  double port = 5;
  std::cout<<"Which port are you looking for? \n"<< "1: top left \n" << "2: top right \n" << "3: middle \n" << "4: bottom left \n" << "5: bottom right \n" << std::endl;
  std::cin>>port;
  if(port > 4) return false;
  return (FindPort(port) != std::pair<double, double>(-999999., -999999.));
}

std::pair<double, double> MEMS_old::FindPort(int port){
  double xoffset, yoffset;

  //These offsets will need to be adjusted once port set up is in place
  switch(port){
  case 0:{ //Let's suppose port 0 is the top left port
    // xoffset = -9400; //Based on scan done Aug 5, peak x is at -9272
    // yoffset = -1500; //Based on scan done Aug 5, peak y is at -1308
    xoffset = -8300;
    yoffset = 1400;
    break;
  }
  case 1:{ //Let's suppose port 1 is the top right port
    xoffset = 8000;
    yoffset = -2500;
    break;
  }
  case 2:{ //Let's suppose port 2 is the middle port
    xoffset = -2000;
    yoffset = -1000;
    break;
  }
  case 3:{ //Let's suppose port 3 is the bottom left port
    xoffset = -1000;
    yoffset = -1000;
    break;
  }
  case 4:{ //Let's suppose port 4 is the bottom right port
    xoffset = -11500;
    yoffset = -11500;
    break;
  }
  default: {
    std::cerr << "That is not a port, please try again" << std::endl;
    return std::pair<double, double>(-999999., -999999.);
  }
  }

  std::pair<double, double> coords = Scan(xoffset, yoffset, port, coarse);
  int xport = coords.first;
  int yport = coords.second;
  if(xport > -99999.){
    coords = Scan(xport, yport, port, fine);
    int xcore = coords.first;
    int ycore = coords.second;
    int stepsize = 32;
    Ramp(xcore,ycore,stepsize,TIMEDELAY);
  }
  return coords;
}

double MEMS_old::GetScopeMax(){
  double val = -9999;
  if(!scope.is_open()){
    std::cerr << "Lost connection!" << endl;
    return -9999.;
  }
  scope << "TRIG:STATE?" << endl;
  std::string state;
  scope >> state;
  if(state == std::string("TRIGGER")){
    scope << "MEASU:IMMED:VAL?" << std::endl;
    // scope << "MEASU:VMAX?" << std::endl;
    usleep(5000);
    scope >> val;
  }
  return val;
}

void MEMS_old::SwitchPD(bool state){
  dac.gpio.SetPin(8, state);
}

void MEMS_old::ShutDown(){
  Ramp(0,0,32,10000);
  SwitchPD(false);
}


// Input.......Addr
// Y_o.........0
// X_D.........1
// Y_D.........2
// X_o.........3

