#include "GPIOInterface.hh"

using RaspberryPi::GPIOInterface;

bool GPIOInterface::Init()
{
  isOpen = bcm2835_init();
  if(!isOpen){
      printf("bcm2835_init failed. Are you running as root??\n"); //PW - no option for input?
  } else {
      bcm_connections++;
  }
  return isOpen;
}

bool GPIOInterface::SetPUD(const uint8_t pin, const char pud)
{
  int pullmode = BCM2835_GPIO_PUD_OFF;
  switch(pud){
  case 'u': pullmode = BCM2835_GPIO_PUD_UP; break;
  case 'd': pullmode = BCM2835_GPIO_PUD_DOWN; break;
  case 'n': //PW - neutral?
  default: pullmode = BCM2835_GPIO_PUD_OFF;
  }

  uint8_t p;
  if(MapPin(pin, p)){
    bcm2835_gpio_set_pud(p, pullmode);
    return true;
  } else {
    return false;
  }
}

bool GPIOInterface::SetPin(const uint8_t pin, const bool val)
{
  uint8_t p;
  if(MapPin(pin, p)){
    bcm2835_gpio_fsel(p, BCM2835_GPIO_FSEL_OUTP);
    if(val)
      bcm2835_gpio_set(p);
    else
      bcm2835_gpio_clr(p);
    return true;
  } else {
    return false;
  }
}

bool GPIOInterface::ReadPin(const uint8_t pin)
{
  uint8_t p;
  if(MapPin(pin, p)){
    bcm2835_gpio_fsel(p, BCM2835_GPIO_FSEL_INPT);
    return bcm2835_gpio_lev(p);
    std::cout<<"pin is "<< bcm2835_gpio_lev(p)<<std::endl;
  } else {
    return false;
  }
}

bool GPIOInterface::MapPin(const uint8_t p, uint8_t &pout)
{
  try {
    pout = pimap.at(p);
  } catch (const std::out_of_range& oor) {
    std::cerr << "Bad pin number: " << (int)p << '\n';
    return false;
  }
  return true;
}

void GPIOInterface::GpClockOn()
{
  bcm2835_gpio_fsel(4, BCM2835_GPIO_FSEL_ALT0); //Set ALT0 to pin 7 - PW
}
