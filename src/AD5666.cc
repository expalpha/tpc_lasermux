#include <iostream>
#include <iomanip>
#include "AD5666.hh"

bool AD5666::SendCommand(uint8_t cmd, uint8_t addr, uint16_t data, uint8_t special)
{
    if(!spi.isOpen) return false;
    uint32_t message = 0;
    message |= ((cmd & 0xF) << 24);
    message |= ((addr & 0xF) << 20);
    message |= (data << 4);
    message |= (special & 0xF);
    //std::cout << "Sending: " << std::hex << message << std::endl;
    spi.Send(message);
    return true;
}

bool AD5666::SetIntRefDaisy(bool int_ref, bool daisy)
{
  uint8_t special = 0;
  if(int_ref) special |= 1;
  if(daisy) special |= 2;
  return SendCommand(AD_CMD_DCEN_REF, AD_ALL_DACS, 0, special);
}

bool AD5666::SetDACReg(uint8_t addr, uint16_t data)
{
  return SendCommand(AD_CMD_WRITE_REG, addr, data);
}

bool AD5666::Init()
{
  bool success = (gpio.Init() && spi.Init());
  if(success) return gpio.SetPin(15, true);
  return success;
}

bool AD5666::PulseLDAC()
{
  bool success = gpio.SetPin(15, false);
  success &= gpio.SetPin(15, true);
  return success;
}

void AD5666::ExchangeBytes(const char *send_data, char *data_out, uint32_t nbytes){
  spi.ExchangeBytes(send_data,data_out,nbytes);
}
