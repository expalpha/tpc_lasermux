#include "PD.hh"

bool PD::Init(int numAv, double timescale)
{
  switch (acc)
    {
    case phidget:
      v_in = new PhidgetPP::VoltageInput(channel,0,false); //false if you're using a dedicated voltage input phidget and not just the hub
      std::cout << "Initializing phidget" << std::endl;
      if(v_in->AllGood()){
	std::cout << "Success." << std::endl;
	return true;
      } else {
	std::cerr << "Failed to initialize phidget." << std::endl;
	return false;
      }
    case scope_tek: {
      bool success = scope.Connect();
      if(channel == 0) channel++;
      if(success){
	success = scope.SetSettings(channel, timescale, maxV, trigLev, numAv);
      }
      return success;
    }
    default:
      return false;
    }
}

double PD::GetV()
{
  switch (acc)
    {
    case phidget:
      return v_in->GetVoltage();
    case scope_tek:
      return scope.GetV();
    default:
      return -9999.;
    }
}
