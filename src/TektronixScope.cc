#include "TektronixScope.hh"

#include <unistd.h>
#include <iostream>
#include <string.h>

using std::cerr;
using std::cout;
using std::endl;

TektronixScope::~TektronixScope()
{
  if (stream.is_open())
    {
      stream << "LOCK NONE" << endl;
    }
}

double TektronixScope::GetV()
{
  double v = -8888.;
  int busy = 1;
  std::string response;
  if (!stream.is_open())
    {
      std::cerr << "Lost connection!" << endl;
      return -9999.;
    }
  stream << "ACQ:NUMAV?" << endl;
  int nav;
  stream >> response >> nav;
  stream << "ACQUIRE:STOPAFTER SEQUENCE" << endl;
  stream << "ACQUIRE:STATE ON" << endl;
  if(verbose > 1) cout << "Acquiring" << endl;
  while(busy == 1){
    usleep(100000);
    stream << "BUSY?" << endl;
    stream >> response >> busy;
    stream << "ACQ:NUMACQ?" << endl;
    int nacq;
    stream >> response >> nacq;
    if(verbose > 1) cout << '\r' << nacq  << '/' << nav << std::flush;
  }
  if(verbose > 1) cout << endl;

  stream << "MEASU:IMMED:MAXI" << std::endl;
  stream << "MEASU:IMMED:VALUE?" << std::endl;
  usleep(50000);
  stream >> response >> v;

  stream << "ACQUIRE:STATE OFF" << endl;
  return v;

}

bool TektronixScope::CheckConnection()
{
  if (!stream.is_open())
    return false;
  stream << "*IDN?" << std::endl;
  char buf[256];
  usleep(10000);
  stream.getline(buf, sizeof(buf));
  int n = stream.gcount();
  if(verbose > 1) std::cout << "Received " << n << " characters, : " << buf << std::endl;
  return (std::string(buf).find(devID) != std::string::npos);
}

bool TektronixScope::SetSettings(unsigned short channel, double timescale, double maxV, double trigLev, int numAv)
{
  bool success = false;
  std::string dummy;
  stream << "VERBOSE 1; LOCK ALL" << endl;
  stream << "*SAV 5" << endl;
  if(verbose) cout << "Factory reset (takes >= 5 seconds)" << std::flush;
  stream << "FACTORY" << endl;
  for(int i = 0; i < 5; i++){
    sleep(1);
    if(verbose) cout << '.' << std::flush;
  }
  int busy = 1;
  while(busy == 1){
    sleep(1);
    stream << "BUSY?" << endl;
    stream >> dummy >> busy;
    if(verbose) cout << '.' << std::flush;
  }
  if(verbose) cout << endl;

  int opc;
  if(verbose) cout << "Applying settings" << endl;
  stream << "ACQ:MODE AVE"
	 << ";NUMAV " << numAv << endl; //originally 16
  stream << "*OPC?" << endl;
  stream >> opc;

  stream << "TRIG:MAIN:MODE NORMAL"
	 << ";LEVEL " << trigLev
	 << ";EDGE:SOURCE CH" << channel
	 << ";COUP DC"
	 << ";SLOPE RIS" << endl;
  stream << "*OPC?" << endl;
  stream >> opc;

  stream << "MEASU:IMMED:SOURCE CH" << channel
	 << ";:MEASU:IMMED:TYPE MAXI" << endl;
  stream << "*OPC?" << endl;
  stream >> opc;

  stream << "HORIZ:MAIN:SCALE " << timescale
	 << ";POSITION " << 3. * timescale << endl;
  stream << "*OPC?" << endl;
  stream >> opc;

  if(maxV < 0.2){
    cout << "Minimum y-scale is 0.02, larger than requested " << maxV/10. << ", applying that." << endl;
    maxV = 0.2;
  }
  stream << "CH" << channel << ":COUP DC"
	 << ";INV OFF"
	 << ";SCALE " << maxV / 10.
	 << ";POS " << -0.4 * maxV << endl;
  stream << "*OPC?" << endl;
  stream >> opc;
  stream << "SELECT:CH" << channel << " ON" << endl;

  // Just checking a few of these settings, assuming it either works or it doesn't
  stream << "ACQ:MODE?" << endl;

  std::string resp;
  stream >> dummy >> resp;
  success = (resp == std::string("AVERAGE"));
  if(!success)
    cerr << resp << " != " << "AVERAGE" << endl;

  stream << "TRIG:MAIN:EDGE:SOURCE?" << endl;
  stream >> dummy >> resp;

  char buf[10];
  sprintf(buf, "CH%d", channel);
  success &= (resp == std::string(buf));
  if(!success)
    cerr << resp << " != " << buf << endl;

  stream << "MEASU:IMMED:TYPE?" << endl;
  stream >> dummy >> resp;
  success &= (resp == std::string("MAXIMUM"));
  if(!success)
    cerr << resp << " != " << "MAXIMUM" << endl;

  stream << "CH1:SCALE?" << endl;
  double scale;
  stream >> dummy >> scale;
  success &= (scale == maxV / 10.);
  if(!success)
    cerr << scale << " != " << maxV/10. << endl;
   
  stream << "HORIZ:MAIN:SCALE?" << endl;
  double tscale;
  stream >> dummy >> tscale;
  if(tscale != timescale)
    cerr << "Actual time scale: " << tscale << "s" << endl;
   
  if (!success)
    {
      cerr << "Settings are not as expected." << endl;
    }
  return success;
}

bool TektronixScope::Connect()
{
  bool success = false;
  char devname[] = "/dev/usbtmc";
  for (int devnum = 0; devnum < 16; devnum++)
    {
      char dn[128];
      sprintf(dn, "%s%d", devname, devnum);
      printf("Trying Device=%s\n", dn);

      stream.open(dn);
      success = CheckConnection();
      if (success)
	{
	  std::cout << "Success: " << dn << " is open" << std::endl;

	  break;
	}
      else
	{
	  stream.close();
	}
    }
  return success;
}
