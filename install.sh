if sudo apt-get install libcap2 libcap-dev; then
    sudo adduser $USER kmem
    echo 'SUBSYSTEM=="mem", KERNEL=="mem", GROUP="kmem", MODE="0660"' | sudo tee /etc/udev/rules.d/98-mem.rules
    if ! [ -w /dev/mem ]; then
        echo "Please reboot before proceeding"
        exit 1
    fi
    export CFLAGS=-DBCM2835_HAVE_LIBCAP
else
    echo "Couldn't install libcap. Proceed without (executables will need to run as root) [y/n]?"
    read resp
    if [ "$resp" != "y" ]; then
        exit 2
    fi
fi

cd bcm2835-1.*
./configure
make
sudo make install

cd -

if [ $(crontab -l | grep -c $PWD/start_clock.sh) -lt 1 ]; then
    (crontab -l ; echo @reboot $PWD/start_clock.sh) | crontab -
else
    echo crontab entry exists already.
fi
./start_clock.sh

mkdir -p build
cd build
cmake ..
make

echo "There is no need to call the install script again."
echo "From now on, simply call make inside build directory."
